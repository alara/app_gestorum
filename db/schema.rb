# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_31_224003) do

  create_table "categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "key"
    t.string "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customer_virdi", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "customer_id", null: false
    t.string "terminal_id", limit: 20
    t.integer "is_new", limit: 1, default: 0
    t.index ["customer_id"], name: "fk_customer_virdi_1_idx"
    t.index ["is_new"], name: "index4"
    t.index ["terminal_id"], name: "index3"
  end

  create_table "customers", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 60
    t.string "description", limit: 45
    t.integer "payment_id"
    t.string "contact", limit: 45
    t.string "email", limit: 45
    t.string "url", limit: 95
    t.integer "active", limit: 1, default: 1
    t.string "folder", limit: 45
    t.string "user_db", limit: 45
    t.index ["active"], name: "index3"
    t.index ["payment_id"], name: "fk_customers_1_idx"
  end

  create_table "jwt_blacklist", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "jti", null: false
    t.index ["jti"], name: "index_jwt_blacklist_on_jti"
  end

  create_table "payments", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 45
  end

  create_table "roles", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "descripcion"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "subdomain"
    t.integer "role_id", null: false
    t.boolean "active", default: false, null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "id_user_type_idx"
  end

  create_table "work_trackings", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.text "reason"
    t.date "last_initial_date"
    t.date "last_final_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "user_id"
    t.integer "work_id"
  end

  create_table "works", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.date "initial_date"
    t.date "final_date"
    t.integer "category_id"
    t.integer "active", limit: 1, default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "works_drafts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "works_thread_id"
    t.string "draft_file_name"
    t.string "draft_content_type"
    t.integer "draft_file_size"
    t.datetime "draft_updated_at"
    t.string "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "user_id"
  end

  create_table "works_threads", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "work_id"
    t.string "name"
    t.text "description"
    t.date "initial_date"
    t.date "final_date_spring"
    t.string "percentage"
    t.string "viability"
    t.integer "estimate_time"
    t.integer "unity_time"
    t.integer "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "works_threads_drafts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "works_thread_id"
    t.date "last_final_date"
    t.text "reason"
    t.integer "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "works_threads_springs", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.text "description"
    t.date "final_date"
    t.integer "user_id"
    t.string "time_estimate"
    t.integer "works_thread_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "finish", limit: 1, default: 0
    t.string "percentage_sprint", limit: 45
    t.text "solution_error"
    t.string "evidence_file_name"
    t.string "evidence_content_type"
    t.integer "evidence_file_size"
    t.datetime "evidence_updated_at"
  end

  add_foreign_key "customer_virdi", "customers", name: "fk_customer_virdi_1"
  add_foreign_key "customers", "payments", name: "fk_customers_1"
  add_foreign_key "users", "roles", name: "role_id"
end

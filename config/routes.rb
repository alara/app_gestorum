Rails.application.routes.draw do
  root 'application#home'
  post 'employee_data', to: 'service#get_data_employee'
  post 'post_reading_assistance', to: 'service#post_reading_assistance'
  post 'checkSession', to: 'application#check_session'
  get 'enrolled_employee', to: 'service#enrolled_employee'
  get  'employees', to: 'service#get_employees'
  get  'get_auto_check', to: 'service#get_auto_check'
  get 'sec_supplie_items', to: 'service#sec_supplie_items'

  resources :sec_supplies

  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }
end

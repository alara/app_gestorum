class CustomFailureApp < Devise::FailureApp
    def respond
        if request.format == :json
            json_error_response
        else
            json_error_response
        end
    end

    def json_error_response
        self.status = 401
        self.content_type = "application/json"
        self.response_body = [
            {
                success: false, 
                payload: {

                },
                error: {
                    code: '500',
                    message: 'Error en sus credenciales.'
                }
            }
        ].to_json
    end
end

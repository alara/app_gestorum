worker_processes 2
timeout 120
preload_app true
working_directory "/home/gestorumbeta/Rails/api"
pid "tmp/pids/unicorn.pid"
stderr_path "log/unicorn.log"
stdout_path "log/unicorn.log"
listen "/var/tmp/unicorn.api.sock"
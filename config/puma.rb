#!/usr/bin/env puma

# Start Puma with next command:
# RAILS_ENV=production bundle exec puma -C ./config/puma.rb

application_path = '/home/gestorum/Rails/api_gestorumael/'
directory application_path
environment 'production'
daemonize true
pidfile "#{application_path}/tmp/pids/puma.pid"
state_path "#{application_path}/tmp/pids/puma.state"
stdout_redirect "#{application_path}/log/puma.stdout.log", "#{application_path}/log/puma.stderr.log"
bind "unix://#{application_path}/tmp/sockets/gitlab.socket"

class Enterprise < ActiveRecord::Base
    #ASSOCIATIONS
    belongs_to :company
    belongs_to :post_risk
    belongs_to :city
    has_many :employees
    has_many :payrolls
    has_many :department_works, -> { uniq }, :through => :employees
    has_many :payment_periods,  :through => :payrolls
    has_many :profit_centers, -> { uniq }, :through => :department_works
  
    has_many :enterprise_card_elements
  
    belongs_to :payroll
    belongs_to :regimen
    belongs_to :person_type
    belongs_to :cities
    has_many :perception_types_enterprises
    has_many :deduction_types_enterprises
    has_one :stamped_wsdl_client
    has_many :enterprise_employer_registrations
    has_many :work_templates
    has_many :catalog_concept_enterprises
    has_many :catalog_concepts, :through => :catalog_concept_enterprises
    has_many :enterprise_saving_funds
    has_many :enterprise_branches
    has_many :contracts_templates
    has_many :schedules, -> { uniq }, :through => :employees
    has_many :company_process_models, -> { uniq }, :through => :employees
  
      scope :get_indicator_employee, -> (id_enterprise){
      select("dw.name,count(emp.department_work_id)as empleados, pc.name as pc_name")
            .joins("left join employees emp on emp.enterprise_id = enterprises.id")
            .joins("left join department_works dw on dw.id = emp.department_work_id") 
            .joins("left join profit_centers pc on pc.id = dw.profit_center_id")
            .where("enterprises.id = #{id_enterprise} and enterprises.active = 1 and (emp.disable_date IS NULL OR UNIX_TIMESTAMP(emp.disable_date) = 0 OR emp.disable_date > NOW())")
            .group("dw.name, pc_name")
     }
  
     scope :get_indicator_employee_with_branch, -> (id_enterprise, branch_id){
      select("dw.name,count(emp.department_work_id)as empleados, pc.name as pc_name")
            .joins("left join employees emp on emp.enterprise_id = enterprises.id")
            .joins("left join department_works dw on dw.id = emp.department_work_id") 
            .joins("left join profit_centers pc on pc.id = dw.profit_center_id")
            .where("enterprises.id = #{id_enterprise} and enterprises.active = 1 and (emp.disable_date IS NULL OR UNIX_TIMESTAMP(emp.disable_date) = 0 OR emp.disable_date > NOW()) and emp.enterprise_branch_id = ?", branch_id)
            .group("dw.name, pc_name")
     }
  
  
     scope :get_indicator_employee_with_branch_nil, -> (id_enterprise){
      select("dw.name,count(emp.department_work_id)as empleados, pc.name as pc_name")
            .joins("left join employees emp on emp.enterprise_id = enterprises.id")
            .joins("left join department_works dw on dw.id = emp.department_work_id") 
            .joins("left join profit_centers pc on pc.id = dw.profit_center_id")
            .where("enterprises.id = #{id_enterprise} and enterprises.active = 1 and (emp.disable_date IS NULL OR UNIX_TIMESTAMP(emp.disable_date) = 0 OR emp.disable_date > NOW()) and emp.enterprise_branch_id is null")
            .group("dw.name, pc_name")
     }
  
      default_scope { order(:commercial_name) }
      scope :active, -> { where(:active=>true) }
      scope :inactive, -> { where(:active=>false) }
      scope :is_subcontractor, -> { where(:is_subcontractor=>true) }
      scope :not_is_subcontractor, -> { where(:is_subcontractor=>false) }
      scope :get_process_model, -> (enterprise){
            select('em.company_process_model_id as cpm_id, pm.description as description')
            .joins('left join employees em on em.enterprise_id = enterprises.id')
            .joins('left join company_process_models cpm on cpm.id = em.company_process_model_id')
            .joins('left join process_models pm on pm.id = cpm.process_model_id')
            .where('enterprises.id = ? ', enterprise)
            .where('cpm.active = 1')
            .group('cpm_id')
      }
  
    
  
    #METHODS
    def select_display
      commercial_name
    end
    
    def to_s
      commercial_name
    end
  
    def full_location
      address_1.to_s + " " + ext_number.to_s + " Col. " + address_2.to_s
    end
  
    def logo_enterprise_url
      "/assets/no-image.png"
    end
  
  end 
class PayrollIncident < ActiveRecord::Base
  belongs_to :enterprise
  belongs_to :employee
  belongs_to :payroll

  #SCOPES
  scope :it_exists, -> ( payroll_id , enterprise_id , employee_id ){
  	where("payroll_id = #{payroll_id} and enterprise_id = #{enterprise_id} and employee_id = #{employee_id}")
  }

  #METHODS
end

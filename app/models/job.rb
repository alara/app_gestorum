class Job < ActiveRecord::Base
    #SCOPES
    default_scope { order(:name) }
    
    #ASSOCIATIONS
    belongs_to :company
    
    #VALIDATIONS
    validates :name, :presence => true
    before_destroy :have_relations 
    
    #METHODS
    def select_display
      name
    end
    
    def to_s
      name
    end
  
    def have_relations
      if !self.company.employees.where(:job_id => self.id).blank?
        return false
      end
    end
  end
  
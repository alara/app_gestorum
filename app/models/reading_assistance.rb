class ReadingAssistance < ActiveRecord::Base
  #ATTRIBUTES
  attr_accessor :schedule_day_attributes
  attr_accessor :schedule_day_not_assigned
  attr_accessor :already_readed
  attr_accessor :from_web
  attr_accessor :rejected
  attr_accessor :today_params
  attr_accessor :massive
  
  #CONCERNS
  include ReadingAssistanceIncidentsConcern
  #CALLBACKS
  before_validation :set_registration_time
  before_validation :set_schedule_day_attributes
  before_validation :set_cause_if_unspecified
  after_save :notify_resident_entry, :on => :create
  after_save { logger.info( '+++++++++++++++++++++++++++++++++++ESTOY CORRIENDO+++++++++++++++++++++++++++++!' ) }
  after_save :save_incident, :if => :is_not_massive
  
  #SCOPES
  
  scope :in_range_entries, ->(start_date, end_date) { where("date >= ? AND date <= ? AND cause = ?", start_date.to_formatted_s(:db), end_date.to_formatted_s(:db), "entry") }
  scope :today_entries, -> { where("date = curdate() AND cause = ?", "entry") }
  scope :with_delay, -> { where("delay = ?", true) }
  scope :from_actives, -> { joins(:employee).actives }
  scope :has_departure, -> (date_to_s, emp_id) { where(:date => date_to_s.to_formatted_s(:db), :cause => 'departure' , :employee_id => emp_id)}
  
  
  #ASSOCIATIONS
  belongs_to :employee
  belongs_to :user
  belongs_to :schedule_day, optional: true
  belongs_to :concept, optional: true
  has_attached_file :photo,
    :styles => { :original => "x300" },
    :default_url => "no-image.png"
  
  #VALIDATIONS
  validate :can_read?,#,  on: :create, 
    :if => Proc.new { |r| !rejected }
  validates :employee_id, :cause, :date, :registration_time, :presence => true
  validates :schedule_day_id, :presence => true,
    :if => Proc.new { |r| %w{extra_entry extra_departure}.exclude?(r.cause) }
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
  

  before_update :has_delay

  #METHODS  
  def self.script(dates)
    ReadingAssistance.where("date >= '#{dates[0..3]}-#{dates[4..5]}-#{dates[6..7]}' and date <= '#{dates[0..3]}-#{dates[4..5]}-#{dates[8..9]}'").each do |ra|
      ra.save_incident2
    end
  end


  def has_delay
    if self.cause == 'entry'
      self.today_params = { month: date.month, day: date.day, year: date.year }
      rt = self.registration_time.strftime('%Y-%m-%d %H:%M:%S').to_time
      
      if !self.schedule_day.nil? and schedule_day.nil?
        schedule_day = self.schedule_day
      elsif schedule_day.nil?
        if !PaymentPeriod.where('initial_date <= ? AND final_date >= ?', self.date, self.date).blank?
          pp =  PaymentPeriod.where('initial_date <= ? AND final_date >= ?', self.date, self.date)
          planeado = self.employee.plan_employee_schedules.where(:payment_period_id => pp.last.id)
        
          if !planeado.blank?
            schedule_day = planeado.last.schedule.schedule_days.find_by(self.date.wday)
          else
            schedule_day = self.employee.schedule.schedule_days.find_by(self.date.wday)
          end
        end
      end
      et_max_time = schedule_day.entry_time + schedule_day.schedule.start_tolerance.hour
      et_max_time = et_max_time.change(self.today_params).strftime('%Y-%m-%d %H:%M:%S').to_time
      
      @it_has_delay = ''

      if rt > et_max_time
        @it_has_delay = 'true'
      else
        @it_has_delay = 'false'
      end
    end
  end


  def is_not_massive
    if !self.massive
      return true
    end
  end
  #  private
  
  
  def can_read?
    if %w{extra_entry extra_departure}.exclude?(cause)
      rt = registration_time
      if self.schedule_day_attributes.nil? and cause != 'departure'
        schedule_day_not_assigned_error
      else
        self.date = (!self.schedule_day_attributes.nil?)? self.schedule_day_attributes["date"] : self.registration_time.to_date
        self.schedule_day_id =  (!self.schedule_day_attributes.nil?)?  self.schedule_day_attributes["id"] : employee.reading_assistances.where(cause: 'entry').last.schedule_day.id
        
        p_rt = self.registration_time
        
        
        if (self.registration_time.to_date<self.date)
          if (cause != 'entry') 
            self.date = self.date - 1.day
            self.schedule_day_id = (!self.schedule_day_attributes.nil? and !self.schedule_day_id.nil?)?  employee.reading_assistances.where(cause: 'entry').last.schedule_day.id :  self.schedule_day_attributes["id"]
            self.today_params = { month: date.month, day: date.day, year: date.year }
          end
        end

        

        if employee.reading_assistances
          .where("reading_assistances.date = ? AND reading_assistances.cause = ?", date.to_formatted_s(:db), cause).blank?
             
          if self.schedule_day_attributes.nil? 
            schedule_day_attributes = employee.get_current_schedule_day(cause, registration_time)
            self.schedule_day_attributes = schedule_day_attributes
          else
            schedule_day_attributes = self.schedule_day_attributes
          end
          
          if !schedule_day_attributes.nil?
            entry_time = schedule_day_attributes["entry_time"].change(today_params)
            departure_time = schedule_day_attributes["departure_time"].change(today_params)
            departure_time += 1.day if schedule_day_attributes["schedule_is_nightly"]
            entry_time_tolerance = employee.schedule.start_tolerance * 3600.0
            et_max_time = entry_time + entry_time_tolerance
          else
            today_params = { month: date.month, day: date.day, year: date.year } if today_params.nil?
            self.schedule_day_attributes = employee.get_current_schedule_day(cause, self.registration_time)
            self.schedule_day_attributes = employee.get_current_schedule_day(cause, self.registration_time - 1.day ) if self.schedule_day_attributes.nil? 
            schedule_day_attributes = self.schedule_day_attributes
            if schedule_day_attributes.nil?
              #entry_time = employee.schedule.schedule_days.first.entry_time.change(today_params)
              #departure_time = employee.schedule.schedule_days.first.departure_time.change(today_params)
              entry_time = schedule_day.schedule.schedule_days.first.entry_time.change(today_params)
              departure_time = schedule_day.schedule.schedule_days.first.departure_time.change(today_params)
              entry_time_tolerance = employee.schedule.start_tolerance * 3600.0
              et_max_time = entry_time + entry_time_tolerance
            else
              entry_time = schedule_day_attributes["entry_time"].change(today_params)
              departure_time = schedule_day_attributes["departure_time"].change(today_params)
              departure_time += 1.day if schedule_day_attributes["schedule_is_nightly"]
              #entry_time_tolerance = employee.schedule.start_tolerance * 3600.0
              entry_time_tolerance = schedule_day.schedule.start_tolerance * 3600.0
              et_max_time = entry_time + entry_time_tolerance
            end
            
          end

          if cause == "entry"
            if employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", date.to_formatted_s(:db), "departure").empty? or is_nightly_schedule?
              ph_incident = employee.incidents.joins(:incident_type).where("date = ? AND incident_types.letters = ?", date.to_formatted_s(:db), "PH").first
              
              if ph_incident.nil? && rt.strftime('%Y-%m-%d %H:%M:%S') > et_max_time.strftime('%Y-%m-%d %H:%M:%S')
                self.delay = true
              end
            else
              errors.add(:base, I18n.t("departure_already_readed"))
            end

          
          elsif cause == "meal_start"
            

            if is_run_schedule? and schedule_day.schedule.is_run_schedule?
              errors.add(:base, I18n.t("schedule_is_run"))
            else
              if is_run_schedule?
                self.cause = 'departure'
              end
              self.schedule_day_id = employee.reading_assistances.last.schedule_day_id
            end   
            
            if employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", date.to_formatted_s(:db), "entry").empty?
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("entry")))
            end
          elsif cause == "meal_end"
            if is_run_schedule? and schedule_day.schedule.is_run_schedule?
              errors.add(:base, I18n.t("schedule_is_run"))
            else
              if is_run_schedule?
                self.cause = 'departure'
              end
              self.schedule_day_id = employee.reading_assistances.last.schedule_day_id
            end
            if employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", date.to_formatted_s(:db), "meal_start").empty?
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("meal_start")))
            end
          elsif cause == "departure"
            if employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", date.to_formatted_s(:db), "entry").empty?
              previous_assistances = employee.reading_assistances.where("reading_assistances.date = ?", (date - 1.day))
              if previous_assistances.find_by(:cause => "departure").nil?
                if !previous_assistances.find_by(:cause => "entry").nil?
                  self.registration_time += -1.day
                  self.date += -1.day
                  self.schedule_day_id = previous_assistances.find_by(:cause => "entry").schedule_day_id
                else
                  errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("entry")))
                end
              else 
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("entry")))
              end  
            end
          end
        else
          already_readed_error(cause)
        end
      end
    else
      if cause == "extra_entry"

        last_incident_cause = employee.reading_assistances.where('date = ?', self.registration_time.to_date).last
        last_incident_cause = employee.reading_assistances.where('date <= ?', self.registration_time.to_date).last if last_incident_cause.blank? 
        
        if !last_incident_cause.nil?
          cause_aux = 'extra_entry'

          if (last_incident_cause.registration_time + 1.hour >= self.registration_time) and ((self.registration_time - last_incident_cause.registration_time) / 1.hour) < 48
              if last_incident_cause.date == (date - 1.day) 
                cause_aux = 'departure'
                self.date = self.date - 1.day
                self.registration_time += -1.day
              end
          end
          
          if cause_aux == 'departure' 
            if(last_incident_cause.cause == 'extra_departure' || last_incident_cause.cause == 'departure')
              self.date = registration_time.try(:to_date) || employee.department_work.time_zone_time_now.to_date
            elsif last_incident_cause.cause == 'entry' || last_incident_cause.cause == 'meal_end'
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("departure")))
            elsif last_incident_cause.cause == 'extra_entry'
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_departure")))
            elsif last_incident_cause.cause == 'meal_start'
               errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("meal_end")))
            end
          else
          
            if !self.schedule_day_attributes.nil?
              if last_incident_cause.cause == 'entry' || last_incident_cause.cause == 'meal_end'
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("departure")))
              elsif employee.reading_assistances.where('date = ? AND cause = ?', self.registration_time.to_date, 'entry').last.nil?
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("entry")))
              elsif last_incident_cause.cause == 'extra_entry'
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_departure")))
              elsif last_incident_cause.cause == 'meal_start'
                 errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("meal_end")))
              end
            else
              if last_incident_cause.date == date
                if last_incident_cause.cause == 'extra_entry'
                  errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_departure")))
                end
              end
            end
          end          
        else
          self.date = registration_time.try(:to_date) || employee.department_work.time_zone_time_now.to_date
        end
      elsif cause == "extra_departure"
        last_incident_cause = employee.reading_assistances.where('date = ?', self.registration_time.to_date).last
        last_incident_cause = employee.reading_assistances.where('date <= ?', self.registration_time.to_date).last if last_incident_cause.blank? 


        cause_aux = 'extra_entry'

        
        if (last_incident_cause.registration_time + 15.hour >= self.registration_time) and ((self.registration_time - last_incident_cause.registration_time) / 1.hour) < 48
            
            if last_incident_cause.date == (date - 1.day)
              cause_aux = 'departure'
              self.date = self.date - 1.day
              self.registration_time += -1.day if self.registration_time.to_date != self.date
            end
        end
        
        
        if cause_aux == 'departure' 
            if(last_incident_cause.cause == 'extra_extry')
              self.date = registration_time.try(:to_date) || employee.department_work.time_zone_time_now.to_date
            elsif last_incident_cause.cause == 'entry' || last_incident_cause.cause == 'meal_end'
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("departure")))
            elsif last_incident_cause.cause == 'extra_departure' || last_incident_cause.cause == 'departure' 
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_entry")))
            elsif last_incident_cause.cause == 'meal_start'
               errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("meal_end")))
            end         
        else
          if !self.schedule_day_attributes.nil?
            if last_incident_cause.cause == 'entry' || last_incident_cause.cause == 'meal_end'
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("departure")))
            elsif last_incident_cause.cause == 'extra_departure' || last_incident_cause.cause == 'departure' 
              errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_entry")))
            elsif last_incident_cause.cause == 'meal_start'
               errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("meal_end")))
            end
          else
            if (last_incident_cause.date == date) 
              if last_incident_cause.cause != 'extra_entry' 
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_entry")))
              end
            else
              if !(((self.registration_time - last_incident_cause.registration_time) / 1.hour) < 48)
                errors.add(:base, I18n.t("dont_yet_readed", cause: I18n.t("extra_entry")))
              end
            end
          end
        end
      end
    end

    logger.info('.......................................................DEMOLICIÓN......................................')
    logger.info()
    logger.info('.......................................................DEMOLICIÓN......................................')

  end


  def add_extra_hours
    if self.cause == 'departure'
      
      reading_assistances = employee.reading_assistances.where(:date => registration_time.to_date, :cause => 'entry')
      entrancy_hour = 0
      if is_nightly_schedule?
        entrancy_hour = registration_time + 1.day  - reading_assistances[0]['registration_time']
      else
        if registration_time >= reading_assistances[0]['registration_time']
          entrancy_hour = registration_time  - reading_assistances[0]['registration_time']
        else
          entrancy_hour = (registration_time + 1.day)  - reading_assistances[0]['registration_time']
        end
      end
      
      total_worked_secs =  entrancy_hour 
      wday = self.schedule_day.day_id
      
      total_hours_day =  employee.schedule.schedule_days.where(day_id: wday).first.total_hours * 1.hour

      extra_time_sec = 0

      if schedule_day.schedule.is_run_schedule?     
        extra_time_sec = (total_worked_secs - total_hours_day)
      else
        meal_total_secs = (employee.schedule.meal_end_time - employee.schedule.meal_start_time) 
        extra_time_sec = (total_worked_secs - (total_hours_day + meal_total_secs))
      end
        
      if  extra_time_sec > 600
        new_extra_hour_entry = registration_time - extra_time_sec + 1
        new_extra_hour_departure = registration_time

        if new_extra_hour_entry.to_date < new_extra_hour_departure.to_date
          new_extra_hour_entry += 1.day
        end

        common_attributes = attributes.except("id", "delay", "created_at",
            "updated_at", "photo_file_name", "photo_content_type",
        "photo_file_size", "photo_updated_at")
        

        common_attributes = common_attributes.merge("cause" => 'extra_entry' ,
          "registration_time" => Time.parse(new_extra_hour_entry.strftime('%Y-%m-%d %H:%M:%S %z')))
        ReadingAssistance.create(common_attributes)

        common_attributes = common_attributes.merge("cause" => 'extra_departure' ,
            "registration_time" => Time.parse(new_extra_hour_departure.strftime('%Y-%m-%d %H:%M:%S %z')))
        ReadingAssistance.create(common_attributes)
        #Se actualiza la hora en la que se guardara el tiempo extra
        if new_extra_hour_entry.to_date < new_extra_hour_departure.to_date
          self.registration_time += 1.day
          self.registration_time = self.registration_time - extra_time_sec 
        else
          self.registration_time = self.registration_time - extra_time_sec 
        end
      end
    end
  end
  
  def is_run_schedule?
    employee.schedule.is_run_schedule?
  end

  def set_cause_if_unspecified
    #cause = "unspecified"
    #self.cause = "unspecified"
    if cause == "unspecified"
      reading_assistances = employee.reading_assistances.where(:date => registration_time.to_date).order(registration_time: :desc).limit(1);
      
      reading_assistances_previous = ''
      no_departure = ''
      no_meal_start_time = ''
      no_meal_end_time = ''
      no_extra_hour_entry = ''
      no_extra_hour_departure = ''

      employee.reading_assistances.where(:date => registration_time.to_date - 1.day).order(registration_time: :desc).each do |previous_assistance|
        reading_assistances_previous=(previous_assistance['cause'] == 'entry')? previous_assistance : reading_assistances_previous
        no_departure = (previous_assistance['cause'] != 'departure')? no_departure : 'y'
        no_meal_start_time = (previous_assistance['cause'] != 'meal_start')? no_meal_start_time : 'y'
        no_meal_end_time = (previous_assistance['cause'] != 'meal_end')? no_meal_end_time : 'y'
        no_extra_hour_entry = (previous_assistance['cause'] != 'extra_entry')? no_extra_hour_entry : 'y'
        no_extra_hour_departure = (previous_assistance['cause'] != 'extra_departure')? no_extra_hour_departure : 'y'
      end


      if reading_assistances.blank?
        if schedule_day_attributes.nil?
          self.cause = "extra_entry"
        else
          self.cause = "entry"
        end

        if is_run_schedule? 
          if !reading_assistances_previous.blank? and no_departure.blank?
            if (reading_assistances_previous['registration_time']+15.hour >= registration_time)
              set_resgitration_time_nightly
            end
          elsif !no_extra_hour_entry.blank? and no_extra_hour_departure.blank?
            prueba = employee.reading_assistances.where(:date => registration_time.to_date - 1.day, :cause => 'extra_entry').first.registration_time
            if (prueba + 15.hour >= registration_time)
              set_resgitration_time_nightly
              self.cause = 'extra_departure'
            end
          elsif reading_assistances_previous.blank? and employee.get_current_schedule_day(cause, registration_time - 1.day).nil?
            if !no_extra_hour_entry.blank?
              previous_date = self.date - 1.day
              
              extra_entry = employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", previous_date, "extra_entry").count
              
              extra_departure = employee.reading_assistances
              .where("reading_assistances.date = ? AND reading_assistances.cause = ?", previous_date, "extra_departure").count

              if extra_entry == (extra_departure + 1)
                set_resgitration_time_nightly
                self.cause = 'extra_departure'
              else
                self.cause = 'entry'
              end
            end
          end
        else
          if !reading_assistances_previous.blank? and no_departure.blank?
            if (reading_assistances_previous['registration_time']+15.hour >= registration_time)
              set_resgitration_time_nightly
              if no_meal_start_time.blank?
                self.cause = 'meal_start'
              elsif no_meal_end_time.blank?
                self.cause = 'meal_end'
              end
            end
          end
        end
      else
        
        last_reading_assistance = employee.reading_assistances.where(:date => registration_time.to_date).last
        
        if last_reading_assistance.cause == "departure"
          self.cause = "extra_entry"
        elsif last_reading_assistance.cause == "extra_departure"
          if !schedule_day_attributes.nil?
            errors.add(:base, "rejected")
            self.rejected = true
          else
            self.cause = 'extra_entry'
          end
        elsif time_difference(last_reading_assistance.registration_time, registration_time, 86400) >= 1
          case last_reading_assistance.cause
          when "entry" 
            if is_run_schedule? 
              if time_difference(last_reading_assistance.registration_time, registration_time, 86400) >= 1800
                self.cause = "departure"
              else
                errors.add(:base, "La lectura de entrada ya había sido registrada.")
              end
            else
              self.cause = "meal_start"
            end
          when "meal_start"
            self.cause = "meal_end"
          when "meal_end"
            self.cause = "departure"
          when "extra_entry"
            self.cause = "extra_departure"
          end
        else
          if schedule_day_attributes.nil?
            errors.add(:base, "rejected by too often with last: #{last_reading_assistance.cause}")
            self.rejected = true
          end
        end

      end
    end
  end
  
  def set_registration_time

    if registration_time.nil?
      self.registration_time = employee.department_work.time_zone_time_now if !from_web
    end

    self.date = registration_time.to_date
    self.today_params = { month: date.month, day: date.day, year: date.year }    
  end

  def set_resgitration_time_nightly
    self.cause = 'departure'
    self.registration_time = self.registration_time - 1.day
  end
  
  def set_schedule_day_attributes
    cause == "departure"
    self.schedule_day_attributes = employee
    .get_current_schedule_day( cause, registration_time)
  end

  def notify_resident_entry
    if cause == "entry" && !employee.user.nil? && employee.user.role.is_resident
      employee_id = employee.id
      
      employee.department_work.company_branch.users.except_user(employee.user.id).each do |u|
        u.notifications.create(
          employee_id: employee_id,
          description: I18n.t(
            "has_read_cause",
            cause: I18n.t(cause)
          ),
          latitude: latitude,
          longitude: longitude
        )
      end
    end
  end
  
  def is_nightly_schedule?
    if !schedule_day_attributes.nil?
      schedule = employee.schedule
      return schedule.is_nightly if !schedule.nil?
    end
    
    false
  end
  
  def time_difference(initial_time, final_time, base)
    seconds = final_time - initial_time
    if seconds < 0
      seconds += base
    else
      seconds
    end
  end
  
  def already_readed_error(cause_)
    errors.add(:base, I18n.t("employee_already_readed", :cause => I18n.t(cause_)))
    @already_readed = true
  end
  
  def schedule_day_not_assigned_error
    errors.add(:base, I18n.t("schedule_day_not_assigned", :employee_name => employee.to_s))
    @schedule_day_not_assigned = true
  end
  
  def after_initialize
    @already_readed = false
    @schedule_day_not_assigned = false
  end
end
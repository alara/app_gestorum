class PlanEmployeeSchedule < ActiveRecord::Base

	attr_accessor :skip_validation

	default_scope { order(:payment_period_id) }

	scope :is_planed, -> { where(planed: true) }

	scope :get_schedules_planed, ->(date) { 
		joins("left join payment_periods on  payment_periods.id=plan_employee_schedules.payment_period_id")
		.where(planed: true)
		.where("payment_periods.initial_date=?",date) 
	}

	scope :get_currente_process, ->(id) { 
		joins("left join payment_periods on  payment_periods.id=plan_employee_schedules.payment_period_id")
		.where("employee_id=?",id)
		.where("? between payment_periods.initial_date AND payment_periods.final_date",Date.today)
	}

	belongs_to :employee 
	belongs_to :schedule 
	belongs_to :payment_period
	#belongs_to :user

	validate :repeat_dates?, unless: :skip_validation

	def to_s
		schedule.to_s
	end

	def repeat_dates?

		has_this_period = PlanEmployeeSchedule.where("employee_id=?",employee_id).
		where("payment_period_id=?",payment_period_id)

	    if has_this_period.count>0
	      errors.add(:employee_id, ' Seleccione otro proceso, este ya ha sido seleccionado anteriormente.')
	  	end
  	end
	
end

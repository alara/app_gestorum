module ReadingAssistanceIncidentsConcern
  def save_incident
    incident = employee.incidents.find_by(:date => date)
    case cause
    when "entry"
      
      entry = employee.reading_assistances.find_by(date: date, cause: "entry")
      incident_type_re = IncidentType.find_by(letters: (entry.delay)? "R" : "RE")

      if incident.nil?
        employee.incidents.create(incident_type_id: incident_type_re.id, date: date, user_id: user_id)
      else
        
        if !@it_has_delay.nil?
          
          @aux_departure = (ReadingAssistance.has_departure(date, employee.id).last)
          
          has_departure = (!@aux_departure.nil?)? true : false
          
          if has_departure
            worked_secs = time_difference(entry.registration_time, @aux_departure.registration_time, 86400)
            is_incomplete = calculate_hours_worked(worked_secs, incident)
            
            if @it_has_delay == 'true'
              if is_incomplete[1]
                incident_type_re = IncidentType.find_by(letters:"JI")
                incident.update(incident_type_id: IncidentType.find_by(letters:"R").id)
              else
                incident_type_re = IncidentType.find_by(letters:"R")
              end
            else
              incident_type_re = IncidentType.find_by(letters: ((!is_incomplete[1])? "A" : "JI"))
            end
            
            incident.update(hours_worked: (is_incomplete[0]))
          else
            if @it_has_delay == 'true'
              incident_type_re = IncidentType.find_by(letters:"R")
            else
              incident_type_re = IncidentType.find_by(letters:"RE") if incident.incident_type.id == 20
            end
          end

          incident.update(incident_type_id: incident_type_re.id, user_id: user_id)
        end
      end
    when "meal_start"
      entry = employee.reading_assistances.find_by(date: date, cause: "entry")
      incident_type_a = IncidentType.find_by(letters: (entry.delay)? "R": "A")
      if !incident.nil?
        worked_secs = time_difference(entry.registration_time, registration_time, 86400)
        hours_worked = (worked_secs / 3600.0) #+ employee.schedule.start_tolerance
        
        incident.update(incident_type_id: incident_type_a.id, hours_worked: hours_worked)
      end
    when "departure"

      meal_start = employee.reading_assistances.find_by(date: date, cause: "meal_start")

      if meal_start.nil?
        date_aux = date
        entry = employee.reading_assistances.find_by(date: date_aux, cause: "entry")
        incident_type_a = IncidentType.find_by(letters: (entry.delay)? "R": "A")
        incident_type_a = (IncidentType.find_by(letters: (@it_has_delay == 'true')? "R" : "A")  )if !@it_has_delay.nil?
        
        if !incident.nil?
          
          worked_secs = time_difference(entry.registration_time, registration_time, 86400)

          if !employee.schedule.meal_start_time.nil? && 
              !employee.schedule.meal_end_time.nil? && 
              registration_time.strftime("%T") >= employee.schedule.meal_end_time.strftime("%T")
            worked_secs -= time_difference(employee.schedule.meal_start_time, employee.schedule.meal_end_time, 86400)
          end
        

          
          
          is_incomplete =  calculate_hours_worked(worked_secs, incident)


          hours_worked = is_incomplete[0]

          if !@it_has_delay.nil?
            if @it_has_delay == 'true'
              incident_type_re = IncidentType.find_by(letters:"JI")
              incident.update(previous_incident_type_id: IncidentType.find_by(letters:"R"))
            end
          end

          
          if schedule_day.total_hours > hours_worked and is_incomplete[1]
            incident_type_a = IncidentType.find_by(letters: "JI")
          end
          
          
          incident.update(incident_type_id: incident_type_a.id, hours_worked: hours_worked)
        end
      else
        meal_end = employee.reading_assistances.find_by(date: date, cause: "meal_end")
        if !incident.nil? && !meal_end.nil?
          worked_secs = time_difference(meal_end.registration_time, registration_time, 86400)
          hours_worked = (worked_secs / 3600.0) + incident.hours_worked
          
          if incident.incident_type.letters != "R" &&
              schedule_day.total_hours > hours_worked
            incident_type = IncidentType.find_by(letters: "JI")
            incident.update(incident_type_id: incident_type.id, hours_worked: hours_worked)
          else
            incident.update(hours_worked: hours_worked)
          end
        end
      end
    when "extra_entry"
      
      if incident.nil?
        incident_type_he = IncidentType.find_by(letters: "HE")
        employee.incidents.create(:date => date, incident_type_id: incident_type_he.id, user_id: user_id)
      else
          if !(created_at == updated_at)
            entries = employee.reading_assistances.where(date: date, cause: "extra_entry")
            registration_time_array = entries.pluck(:id) if !entries.blank?
            extra_hours_worked = incident.extra_hours_worked.to_f 
            if self.registration_time_changed?
              val_index = entries.pluck(:id).index(self.id)
              extra_departure = employee.reading_assistances.where(date: date, cause: "extra_departure")[val_index]
              worked_secs = (extra_departure.blank?)? 0 : time_difference(registration_time, extra_departure.registration_time, 86400)
              worked_secs_last = (extra_departure.blank?)? 0 : time_difference( (self.changes["registration_time"][0]),extra_departure.registration_time, 86400)
              dif = worked_secs - worked_secs_last.to_f
              extra_hours_worked += (dif / 3600.0)
              incident.update_attribute(:extra_hours_worked, extra_hours_worked)
            end
          end        
      end
    when "extra_departure"
      if !(created_at == updated_at)
        departures = employee.reading_assistances.where(date: date, cause: "extra_departure")
        registration_time_array = departures.pluck(:id) if !departures.blank?
        extra_hours_worked = incident.extra_hours_worked.to_f 

        if self.registration_time_changed?
          val_index = departures.pluck(:id).index(self.id)
          extra_entry = employee.reading_assistances.where(date: date, cause: "extra_entry")[val_index]
          worked_secs = time_difference(extra_entry.registration_time, registration_time, 86400)
          worked_secs_last = time_difference(extra_entry.registration_time, (self.changes["registration_time"][0]), 86400)
          dif = worked_secs - worked_secs_last.to_f
          extra_hours_worked += (dif / 3600.0)
        end

        incident.update_attribute(:extra_hours_worked, extra_hours_worked)
      else
        extra_entry = employee.reading_assistances.where(date: date, cause: "extra_entry").last
        worked_secs = (extra_entry.blank?)? 0 : time_difference(extra_entry.registration_time, registration_time, 86400)
        extra_hours_worked = incident.extra_hours_worked + (worked_secs / 3600.0)
        incident.update(:extra_hours_worked => extra_hours_worked)
      end
    end
  end

  def save_incident2
    incident = employee.incidents.find_by(:date => date)
    case cause
    when "meal_start"
      entry = employee.reading_assistances.find_by(date: date, cause: "entry")
    
      if !incident.nil?
        worked_secs = time_difference(entry.registration_time, registration_time, 86400)
        hours_worked = (worked_secs / 3600.0)# + employee.schedule.start_tolerance
        
        incident.update(hours_worked: hours_worked)
      end
    when "departure"
      meal_start = employee.reading_assistances.find_by(date: date, cause: "meal_start")
      if meal_start.nil?
        entry = employee.reading_assistances.find_by(date: date, cause: "entry")
        if !incident.nil?
          worked_secs = time_difference(entry.registration_time, registration_time, 86400)
          if !employee.schedule.meal_start_time.nil? && 
              !employee.schedule.meal_end_time.nil? && 
              registration_time.strftime("%T") >= employee.schedule.meal_end_time.strftime("%T")
            worked_secs -= time_difference(employee.schedule.meal_start_time, employee.schedule.meal_end_time, 86400)
          end
          #hours_worked = (worked_secs / 3600.0) + employee.schedule.start_tolerance + employee.schedule.end_tolerance
          
          is_incomplete =  calculate_hours_worked(worked_secs, incident)

          hours_worked = is_incomplete[0]


          incident.update(hours_worked: hours_worked)
        end
      else
        meal_end = employee.reading_assistances.find_by(date: date, cause: "meal_end")
    
        if !incident.nil? && !meal_end.nil?
          worked_secs = time_difference(meal_end.registration_time, registration_time, 86400)
          hours_worked = (worked_secs / 3600.0) + incident.hours_worked
          
          incident.update(hours_worked: hours_worked)
        end        
      end
    when "extra_entry"
      if incident.nil?
        incident_type_he = IncidentType.find_by(letters: "HE")
        employee.incidents.create(:date => date, incident_type_id: incident_type_he.id, user_id: user_id)
      end
    when "extra_departure"
      extra_entry = employee.reading_assistances.find_by(date: date, cause: "extra_entry")
      worked_secs = time_difference(extra_entry.registration_time, registration_time, 86400)
      extra_hours_worked = incident.extra_hours_worked + (worked_secs / 3600.0)
      incident.update(:extra_hours_worked => extra_hours_worked)
    end
  end
end



def calculate_hours_worked(worked_secs, incident)
  payment_period = PaymentPeriod.with_date(date, date, employee.company_process_model_id).first
  schedule = employee.schedule
  is_incomplete = false

  
  if !payment_period.nil?
    plan_schedule = employee.plan_employee_schedules.find_by_payment_period_id(payment_period.id)
    
    if !plan_schedule.nil?
      schedule = plan_schedule.schedule 
    end

    if schedule.nil?
      schedule = employee.schedule
    end
  end
  
  wday = (date.wday == 0)? 7 : date.wday  
  
  day_data = schedule.schedule_days.where(day_id: wday).pluck(:total_hours, :entry_time, :departure_time).first
  if day_data.blank?
    day_data = schedule.schedule_days.pluck(:total_hours, :entry_time, :departure_time).first
  end
  
  total_hours =  (day_data.blank?)? 0 : day_data[0]
  
  
  if (total_hours * 3600) > worked_secs
    entry = day_data[1].to_datetime
    departure = day_data[2].to_datetime 
    
    time_dif = Time.now.strftime('%H:%M:%S').to_time - Time.current.strftime('%H:%M:%S').to_time
  
    entry_time = employee.reading_assistances.where(date:date, cause:'entry').first.registration_time.strftime("%Y-%m-%d %H:%M:%S").to_time
    if @it_has_delay.nil?
      departure_time = self.registration_time.to_time + (time_dif).hour
    else
    
      departure_time = @aux_departure.registration_time.to_time  + (time_dif).hour if !@aux_departure.blank?
    end
    departure = departure.change(year: departure_time.year, month: departure_time.month, day:departure_time.day).strftime("%Y-%m-%d %H:%M:%S").to_time
    entry = entry.change(year: date.year, month: date.month, day:date.day).strftime("%Y-%m-%d %H:%M:%S").to_time  
    
    dif_entry = dif_departure = 0


    if departure_time < departure
      dif_departure = (departure - departure_time).to_f 
      is_incomplete = true
    end

    

    if incident.incident_type_id == 15   
     
      if entry_time > entry
        dif_entry = (entry_time - entry)    
      end
    
      if dif_entry > 0 
        if (dif_departure/3600) <= schedule.end_tolerance and schedule.end_tolerance > 0
          worked_secs += dif_entry + dif_departure
          is_incomplete = false  if (departure_time + (dif_departure)) >= departure
        end
      end
    elsif incident.incident_type_id == 20
    
      if (total_hours * 3600) > worked_secs and schedule.end_tolerance > 0
        dif_departure = (departure - departure_time).to_f 
        if dif_departure > 0 and (dif_departure/3600) <= schedule.end_tolerance
          worked_secs += dif_departure
          is_incomplete = false if (departure_time + (dif_departure)) >= departure
        end
      end
    else 
      
      dif_entry = (entry_time - entry) 
      if (total_hours * 3600) > worked_secs and dif_entry > 0 and ((schedule.start_tolerance * 60).round) >= dif_entry / 60
        worked_secs += dif_entry
      end

      if (total_hours * 3600) > worked_secs and schedule.end_tolerance > 0
        
        dif_departure = (departure - departure_time).to_f 
        if dif_departure > 0 and (dif_departure/3600) <= schedule.end_tolerance
          worked_secs += dif_departure + dif_entry
          is_incomplete = false if (departure_time + (dif_departure)) >= departure
        end
      end
    end

    
    if (total_hours * 3600) < worked_secs and (dif_entry > 0 or dif_departure > 0)
      worked_secs = total_hours * 3600
    end

  end 
  
  
  [worked_secs.to_f / 3600, is_incomplete]
end
class SubDepartmentCrew < ActiveRecord::Base
    #SCOPES
    default_scope { order("id DESC") }
    
    #ASSOCIATIONS
    belongs_to :company
    belongs_to :employee
    has_many :employees
    has_many :reading_assistances, :through => :employees
    #METHODS
    def select_display
      name
    end
    
    def to_s
      name
    end
  end
  
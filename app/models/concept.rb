class Concept < ActiveRecord::Base
    #SCOPES
    default_scope { order(:name) }
    
    #RELATIONS
    belongs_to :company
    has_many :incidents
    
    #VALIDATIONS
    validates :name, :presence => true
    
    #METHODS
    def select_display
      name
    end
    
    def to_s
      name
    end
  end
  
class Day < ActiveRecord::Base
    #ASSOCIATIONS
    has_many :schedules
    #METHODS
  
  
    def select_display
      I18n.t(name).capitalize
    end
    
    def to_s
      select_display
    end
  end
  
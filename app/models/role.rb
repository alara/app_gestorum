class Role < ActiveRecord::Base
    #VALIDATIONS
    has_many :permissions
  
    scope :get_id_by_name, ->(rol){ where("name = ?", rol) }
    scope :auto_filter, ->(term){ where("name LIKE ?", "%#{term}%") }
    
    #METHODS
    def select_display
      name
    end
  end
  
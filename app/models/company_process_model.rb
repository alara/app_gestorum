class CompanyProcessModel < ActiveRecord::Base

    #SCOPES
  
    #RELATIONS
    belongs_to :company
    belongs_to :process_model
    has_many :payment_periods
    has_many :employees
    
    #VALIDATIONS
    validates :company_id, :process_model_id, :initial_day, :presence => true
    validates :process_model_id, uniqueness: { scope: :company_id }
    
    #CALLBACKS
    before_update :process_cannot_be_modified
    
    #METHODS 
    def process_cannot_be_modified
      if payment_periods.any?
        errors.add(:base, I18n.t("process_cannot_be_modified"))
        false
      end
    end
    
    def select_display
      process_model
    end
    
    def to_s
      process_model
    end
  
  
    def self.create_extraordinary(company)
      process_models = ProcessModel.where("process_models.key=?",99).first
      company_process_model = self.new(
        :company_id => company, 
        :process_model_id => process_models.id, 
        :active => 1, 
        :initial_day => 4)
      company_process_model.save
      company_process_model
    end
  
    def self.get_another_periodicity(company)
      company_process_model = self.joins("LEFT OUTER JOIN process_models ON process_models.id = company_process_models.process_model_id")
      .where("process_models.key = ?", 99)
      .first
      company_process_model = self.create_extraordinary(company) if company_process_model.blank?
      company_process_model
      
    end
  
  end
  
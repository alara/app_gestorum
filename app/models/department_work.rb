class DepartmentWork < ActiveRecord::Base
    #include DepartmentWorkAssistancesD
    #SCOPES
    default_scope -> { order(:name) }
    scope :active, -> { where(:active=>true) }
    
    #ASSOCIATIONS
    belongs_to :company_branch
    belongs_to :employee
    belongs_to :profit_center
    has_many :subdepartments  
    has_many :employees, :dependent => :restrict_with_error
    #has_many :payrolls
    has_many :virdi_terminals
    has_many :loans, :through => :employees
    has_many :fixed_discounts, :through => :employees
    has_many :fixed_bonuses, :through => :employees
    has_many :sub_department_crews, :through => :employees
    has_many :reading_assistances, :through => :employees
    has_many :incidents, :through => :employees
    has_many :schedules, :through => :employees
    has_many :company_process_models, :through => :employees
    has_many :work_templates, :dependent => :destroy
    has_many :department_work_policies_bonus, :dependent => :destroy
  
    has_many :budget_paths, :as => :budgetable #polymorphic 

    def select_display
        name
      end
      
      def to_s
        name
      end

      def profit_name
        result = name
        result = "#{profit_center.name} | #{name}" if !profit_center.nil?
        result
      end
      def time_zone_time_now
        Time.now.in_time_zone(time_zone)
      end
end
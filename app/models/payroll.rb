class Payroll < ActiveRecord::Base
  #ATTRIBUTES
  attr_accessor :skip_validation, :total_employees_sub, :is_current, :skip_validation_closed

  #SCOPES
  default_scope { order(:payment_period_id) }
  scope :opened, -> { where(:closed => false) }  

  scope :closed, -> { where(:closed => true) } 

  scope :has_saving, ->(enterprise_id){ 
    joins("left outer join payroll_descriptions on payroll_descriptions.id=payrolls.payroll_description_id")
    .where("payroll_descriptions.payroll_type=?","FON")
    .where("payrolls.enterprise_id=?",enterprise_id)
   } 

  scope :current_payroll_by_process, -> (enterprise_id,company_process_model_id){
    joins("left outer join payment_periods on payment_periods.id=payrolls.payment_period_id")
    .where("payrolls.enterprise_id=?",enterprise_id)
    .where("payment_periods.company_process_model_id=?",company_process_model_id)
    .where("payment_periods.initial_date <=? AND payment_periods.final_date >=?", Date.today, Date.today)
    .first
   }
  
   scope :from_date, ->(enterprise_id,company_process_model_id,id){
    
    where("payrolls.enterprise_id=?",enterprise_id)
    .where("payment_periods.company_process_model_id=?",company_process_model_id)
    .where("payrolls.id>=?",id)
    .joins("left outer join payment_periods on payment_periods.id=payrolls.payment_period_id")
   }

  scope :current_payroll, -> { 
    joins("left outer join payment_periods on payment_periods.id=payrolls.payment_period_id")
    .where("? between payment_periods.initial_date AND payment_periods.final_date",Date.today)
    .first
   }  


  scope :by_enterprise, ->(term){ 
    select("payrolls.id as payroll_id,enterprises.commercial_name,payment_periods.*")
    .joins("left outer join enterprises on enterprises.id=payrolls.enterprise_id")
    .joins("left outer join payment_periods on payment_periods.id=payrolls.payment_period_id")
    .where("enterprises.commercial_name LIKE ? OR payment_periods.number_payment LIKE ? OR payment_periods.initial_date LIKE ? OR payment_periods.final_date LIKE ?", "%#{term}%", "%#{term}%", "%#{term}%","%#{term}%") 
    .where("closed=?",true)
  }
    

  scope :with_date, ->(initial_date, final_date){
    where("payment_periods.initial_date <=? AND payment_periods.final_date >=?", final_date, initial_date)
    .joins('LEFT JOIN payment_periods ON `payment_periods`.id = payrolls.payment_period_id')
    .where('payroll_description_id = 1')
  }

  scope :period_by_enterprise, ->(enterprise_id,initial_date, final_date){
    with_date(initial_date, final_date)
    .where('enterprise_id= ?',enterprise_id)
  }

  scope :period_by_branch, ->(enterprise_branch_id,initial_date, final_date){
    with_date(initial_date, final_date)
    .where(:enterprise_branch_id=>enterprise_branch_id)
  }

  scope :with_payment, -> (payment, enterprise_id){
    where("payment_period_id = ? AND enterprise_id = ?", payment, enterprise_id)
  }

  scope :with_process, -> (process){
    joins('left join payment_periods pay ON pay.id = payrolls.payment_period_id')
    .where('pay.company_process_model_id = ?', process)
  }

  scope :last_payrolls, -> (enterprise_id) {
    where('enterprise_id= ?',enterprise_id)
    .opened
    .normal
  }

  scope :last_payrolls_branch, -> (enterprise_branch_id) {
    where('enterprise_branch_id= ?',enterprise_branch_id)
    .opened
    .normal
  }



  scope :normal, ->{
    where('payroll_description_id = 1')
  }
  

  scope :first_payroll_open, -> (process){
    joins('left join payment_periods pay ON pay.id = payrolls.payment_period_id')
    .where('payrolls.closed = 0')
    .where('year(pay.initial_date) = year(now())')
    .where('pay.company_process_model_id = ?', process)
    .where('payroll_description_id = 1')
  }

  scope :range_dates, ->(company_process_model_id,initial_date,final_date){
  	joins("left join payment_periods on payment_periods.id= payrolls.payment_period_id")
  	.where("payment_periods.linked_process = ? or payment_periods.company_process_model_id =?",company_process_model_id,company_process_model_id)
  	.where("payment_periods.initial_date >= ? and payment_periods.final_date <=?",initial_date,final_date)
  }



  #RELATIONS
  belongs_to :payment_period
  belongs_to :payroll_type
  belongs_to :enterprise
  belongs_to :enterprise_branch
  belongs_to :user

  belongs_to :payroll_description
  
  belongs_to :settlement
  has_many :employee_payrolls
  has_many :employees, ->{ uniq },  :through => :employee_payrolls
  has_many :employees_payrolls_deductions
  has_many :employees_payrolls_perceptions
  has_many :payroll_incidents
  has_many :fonacot_payrolls
  has_many :fonacot_loans, :through => :fonacot_payrolls
  has_many :employee_stampeds
  has_many :infonavit_payrolls
  has_many :department_works, -> { uniq }, :through => :employee_payrolls
  has_many :subdepartments, -> { uniq }, :through => :employee_payrolls
  has_many :infonavit_credits, :through => :infonavit_payrolls
  has_many :taxes_data_employees
  has_many :vacations
  accepts_nested_attributes_for :employee_payrolls
  
  #VALIDATIONS
  validates :date_payment, :closed, :user_id, :total_employees, :presence => true, unless: :skip_validation
  #validate :check_previous_closed, unless: :skip_validation 
  validate :is_already_closed, unless: :skip_validation_closed
  #callback
  
  


  private
  
  #METHODS
  def check_previous_closed
    if Date.current.to_formatted_s(:db) < payment_period.final_date.to_formatted_s(:db) and self.payroll_description.key != "E"
      errors.add(:base, I18n.t("payment_period_not_finished"))
    end
  end

  def to_s 
    payment_period.select_display
  end 
  
  def is_already_closed
    if !self.changes.include?("closed")
      errors.add(:base, "closed")
    end
  end

  
end
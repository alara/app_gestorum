class Holiday < ActiveRecord::Base
  #SCOPES
  default_scope { order(:date) }
  
  #RELATIONS
  belongs_to :company
  
  scope :it_is?, -> (date) {where('date = ?', date)}

  #VALIDATIONS
  validates :name, :date, :presence => true
  validates :date, uniqueness: { scope: :company_id }
end

class Incident < ActiveRecord::Base
    #ATTRIBUTES
    attr_accessor :extra_hours_added_formatted, :days_quantity
    
    #SCOPES
    default_scope -> { order(date: :desc) }
    scope :absences, -> { joins(:incident_type).where("incident_types.letters = 'F'") }
    scope :absences_year, -> { joins(:incident_type).where("incident_types.letters = 'F'").where("YEAR(incidents.date) =YEAR(CURDATE())") }
    scope :with_salary_goce, -> { joins(:incident_type).where("incident_types.with_salary_gose = 1") }
    scope :get_work_absenteeism, -> (department_id,year){
       select("year(incidents.date) as anio ,MONTH(incidents.date) as mes,sum(scd.total_hours) as work_absenteeism, round(sum(distinct(sc.total_hours * 4.35)),2) as schedule_total_hours")
        .joins("inner join employees em on em.id = incidents.employee_id")
        .joins("inner join schedules sc on sc.id = em.schedule_id")
        .joins("inner join schedule_days scd on scd.schedule_id = em.schedule_id")
        .where("incidents.incident_type_id = 14 and WEEKDAY(incidents.date)+1 = scd.day_id and 
               scd.schedule_id = em.schedule_id and year(incidents.date)= #{year} and 
               sc.company_branch_id = 1 and sc.is_run_schedule = 1 and 
               em.department_work_id = #{department_id} and 
               (em.disable_date IS NULL OR UNIX_TIMESTAMP(em.disable_date) = 0 OR em.disable_date > CURDATE())")
        .group("year(incidents.date),month(incidents.date)")
    }
  
    scope :get_incidents_to_delete, -> (employee_id,date_initial){
        where("employee_id = ?",employee_id)
        .where("date>=?",date_initial)
        .joins(:incident_type)
        .where("incident_types.is_delete = 1")
      }
  
    scope :get_by_letters, -> (letters){
        where("letters = ?",letters)
        .joins(:incident_type)
      }
  
  
  
    scope :get_extra_hours, ->(initial,final) { 
      where("date>=?",initial)
      .where("date<=?",final) }
  
    #RELATIONS
    belongs_to :employee
    belongs_to :user
    belongs_to :incident_type
    #belongs_to :accident_type
    #belongs_to :concept
    has_attached_file :evidence,
      :path => ":rails_root/private/system/incidents/evidence/:id/:basename.:extension"
    has_attached_file :evidence2,
      :path => ":rails_root/private/system/incidents/evidence/:id/:basename.:extension"
    
    
    before_save :check_params
    before_save :check_complete_day
    after_save :update_payroll_incidents
    before_save :update_incident_types
    before_save :update_history
    #after_save :save_incident_massive_incidents  
      
  
    #after_save :update_payrolls
    #METHODS  

    def update_history
      new_element = {
        :letter => self.incident_type.name,
        :name => self.incident_type.letters,
        :date => Time.current.strftime("%d-%m-%Y %H:%M %p"),
        :user => self.user.username
      }

      if self.changes.include?("incident_type_id")
        if self.history.blank?
          #self.history = "#{self.incident_type.name} #{self.incident_type.letters} #{(self.updated_at.blank?)? Time.now.strftime("%d-%m-%Y %H:%M %p") : self.updated_at.strftime("%d-%m-%Y %H:%M %p")} #{self.user.username}"
          self.history = [new_element].to_s
        else
          self.history = (eval(self.history) << new_element).to_s
        end
      end
    end

  
    def update_incident_types
      self.inability_type_id = nil if self.incident_type_id != 12
      self.inability_cause_id = nil if self.incident_type_id != 12
    end
    
    def check_params
      if self.hours_added.blank?
        self.hours_added = 0.0
      end
      
      if self.incident_type_id_changed?
        self.previous_incident_type_id = self.incident_type_id_was
      end
      
      if self.extra_hours_added_formatted.nil?
        self.extra_hours_added = 0
      else
        hours_array = self.extra_hours_added_formatted.split(":")
        self.extra_hours_added = (hours_array[0].to_f * 60 + hours_array[1].to_f) / 60
      end
    end
  
    def check_complete_day
      if self.incident_type.id == 23
        payroll = PaymentPeriod.with_date(date, date, self.employee.company_process_model_id).first
        planned_schedule = employee.plan_employee_schedules.find_by_payment_period_id(payroll)
        
        schedule = planned_schedule.schedule if !planned_schedule.nil? 
        schedule = self.employee.schedule if planned_schedule.nil?
  
        wday = self.date.wday
        wday = 7 if wday == 0
  
        total_hours = 0
        total_hours = schedule.schedule_days.find_by_day_id(wday).total_hours if !schedule.schedule_days.find_by_day_id(wday).blank?
       
  
        if self.hours_worked <= total_hours
          self.hours_worked = total_hours
        end
  
      end
    end
    
    def incident_not_exists
      if !employee.nil?
        incidents = employee.incidents.where("date = ?", date)
        incidents = incidents.where.not(id: id) if !id.nil?
        
        if incidents.any?
          errors.add(:base, I18n.t("incident_already_exists", :date => date))
        end
      end
    end
  
    
    private
  
    def update_payroll_incidents
      total_hours = 0
      totals = [0, 0, 0, 0]
      incident_is = nil
      array_incidents = []
      previous_incident_is = nil
      previous_time_worked = 0.0
      previous_eh = 0.0
      paid_incapacities = 0
      paid_festives = 0.0
  
  
  
      payment  = PaymentPeriod.specific(employee.company_process_model_id, self.date)
      if !payment.blank?
        payment_id = payment.last.id.to_i
        enterprise_id = employee.enterprise_id.to_i
  
  
        parent_id = nil 
  
      
        if !employee.enterprise_branch_id.blank?
          if !employee.enterprise_branch.has_payroll
            if !employee.enterprise_branch.parent_id.blank?
              is_parent = false
              parent =  employee.enterprise_branch.parent
  
              if !parent.has_payroll
                while !is_parent
                  if !parent.parent_id.blank?                  
                    if parent.parent.has_payroll
                      is_parent = !is_parent
                    end
                    parent = parent.parent
                  else
                    is_parent = !is_parent
                    parent = nil
                  end              
                end
              end          
              parent_id =  (parent.blank?)? nil : parent.id 
            end
          else
            parent_id =  employee.enterprise_branch_id
          end
        end
  
        #payroll = Payroll.with_payment(payment_id, enterprise_id).last
        payroll = Payroll.where(:enterprise_branch_id => parent_id).with_payment(payment_id, enterprise_id)
  
        exists = PayrollIncident.it_exists(payroll.last.id, enterprise_id, employee.id) if !payroll.blank?
        
        assistances_array = [13, 10, 17, 18, 22, 23, 25, 26, 27]
        permissions_array = [20, 26, 27]
        absences_array = [14, 11, 15, 24]
  
        more_than_one = false
  
  
  
        
  
        if !exists.blank? and exists.count == 1
          exists = exists.last
          totals[0] = (exists.assistances.blank?)? 0 : exists.assistances  #assistances
          totals[1] = (exists.absences.blank?)? 0 : exists.absences  #absences 
          totals[2] = (exists.incapacities.blank?)? 0 : exists.incapacities  #incapacities
          totals[3] = (exists.vacations.blank?)? 0 : exists.vacations  #vacations
          previous_time_worked = exists.total_hours
          previous_eh = exists.extra_hours.to_f
          previous_eh = exists.paid_incapacities
          paid_festives = exists.paid_festives
          array_incidents =  eval(exists.discounts) if !exists.discounts.blank?
        else
           if !exists.blank? and exists.count > 1
            more_than_one  = true
           end
        end
  
        if assistances_array.include?(self.incident_type_id)  or permissions_array.include?(self.incident_type_id)
          incident_is = 0
        elsif absences_array.include?(self.incident_type_id)
          incident_is = 1
        elsif self.incident_type_id == 12
          incident_is = 2
        elsif self.incident_type_id == 19
          incident_is = 3
        elsif self.incident_type_id == 21 
          wday = self.date.wday
          if employee.schedule.schedule_days.pluck(:day_id).include?(wday)
            incident_is = 0
          else
            incident_is = 5
          end
        end
  
  
        if !self.incident_type_id.blank?
          if assistances_array.include?(self.previous_incident_type_id) or permissions_array.include?(self.previous_incident_type_id) 
            previous_incident_is = 0
          elsif absences_array.include?(self.previous_incident_type_id)
            previous_incident_is = 1
          elsif self.previous_incident_type_id == 12
            previous_incident_is = 2
          elsif self.previous_incident_type_id == 19
            previous_incident_is = 3
          elsif self.previous_incident_type_id == 21 
            wday = self.date.wday
            if employee.schedule.schedule_days.pluck(:day_id).include?(wday)
              previous_incident_is = 0
            end
          end
        end
  
        previous_incident_letters =  "" 
        logger.info('Detalle Jna............................................................................')
        logger.info(!self.previous_incident_type_id.blank?)
        logger.info(self.previous_incident_type_id)
        previous_incident_letters =  IncidentType.find_by_id(self.previous_incident_type_id.to_s).letters if !self.previous_incident_type_id.to_s.blank?
        
        incident_letters = self.incident_type.letters
  
        hours_worked = (self.hours_worked.blank?)? 0.0 : self.hours_worked
        hours_subtracted =(self.hours_added.blank?)? 0.0 : self.hours_added
        hours_added = (self.hours_subtracted.blank?)? 0.0 : self.hours_subtracted
  
        worked_time  = ( hours_worked + hours_added - hours_subtracted)
        if [13, 11, 12, 19, 22, 23, 25].exclude?(self.incident_type_id)
          if employee.schedule.schedule_days.pluck(:day_id).include?(wday)
            day_hrs =  employee.schedule.schedule_days.find_by_day_id(wday).total_hours
  
            discount_time =  Time.at((day_hrs - worked_time) * 3600).utc.strftime("%H:%M:%S") if worked_time < day_hrs
  
            
            apply = [14, 15, 24]
  
            absence_time = day_hrs - worked_time 
            coste = employee.daily_fee / employee.schedule.average_work_time
  
            array_incidents = go_over_array(array_incidents)
  
            is_apply = (apply.include?(self.incident_type_id))? 0 : 1
            is_apply = 1 if self.incident_type_id == 20 and worked_time > 0
          
            if is_apply == 0
              # Séptimo día (Días jornada/Descuento) Se debe ajustar a la función que genere chuy?? para el cálculo del descuento del séptimo día.
              discount_time = 1 + (1/6.to_f )
            end
            
            if !discount_time.blank? and absence_time > 0.0
              array_incidents << {:previous_incident => previous_incident_letters, 
                :incident => incident_letters, :date => self.date, :time => discount_time, 
                :coste => round_number(coste * absence_time),:apply => is_apply}
            end 
          end
        else
          array_incidents = go_over_array(array_incidents)
        end
  
        total_discounts = 0
  
        if !array_incidents.blank? or array_incidents != ""
            array_incidents.each_with_index do |discount|
              total_discounts = total_discounts + discount[:coste].to_f if discount[:apply] == 1 
            end
        end
  
        
        if self.changes.include?("hours_worked") or self.changes.include?("hours_added") or self.changes.include?("hours_subtracted")
          hours_worked_flag = self.changes.include?("hours_worked")? self.changes["hours_worked"][0] : hours_worked
          hours_added_flag = self.changes.include?("hours_added")? self.changes["hours_added"][0] : hours_added
          hours_sub_flag = self.changes.include?("hours_subtracted")? self.changes["hours_subtracted"][0] : hours_subtracted
  
          hours_worked_flag = 0 if hours_worked_flag.blank?
          hours_added_flag = 0 if hours_added_flag.blank?
          hours_sub_flag = 0 if hours_sub_flag.blank?
  
  
  
  
          prev_hours =  (hours_worked_flag + hours_added_flag - hours_sub_flag) 
          
          previous_time_worked = previous_time_worked - ( prev_hours - worked_time ) if !previous_time_worked.blank?
        else
          if previous_incident_is != incident_is and self.changes.include?('incident_type_id') and !incident_is.blank?
            if incident_is == 0 
              previous_time_worked += (worked_time.blank?)? 0.0 : worked_time if !previous_time_worked.blank?
            elsif incident_is == 1 or incident_is == 2 or incident_is == 3
              previous_time_worked -= (worked_time.blank?)? 0.0 : worked_time if !previous_time_worked.blank?
            end
          end
        end
  
        
  
        if self.changes.include?("extra_hours_added")
          
          previous_eh -= self.changes["extra_hours_added"][0] 
          if !self.extra_hours_worked.blank? and self.extra_hours_worked > self.extra_hours_added
            previous_eh += self.extra_hours_added 
          else
            previous_eh +=  self.extra_hours_worked if !self.extra_hours_worked.blank?
            previous_eh +=  0 if self.extra_hours_worked.blank?
          end
  
        end
        
        is_holiday = !Holiday.it_is?(self.date).blank?
  
  
  
        if incident_is == 0 or incident_is == 1
          schedule_days =  self.employee.schedule.schedule_days
          if schedule_days.pluck(:day_id).include?(self.date.wday)
            if worked_time > 0.0
              hrs = schedule_days.find_by(day_id:self.date.wday).total_hours
              total_day = (hours_worked/hrs.to_f)
              paid_festives += (total_day > 1)? 1 : total_day
            end
          end
        else
          if worked_time > 0.0
            schedule_days =  self.employee.schedule.schedule_days if schedule_days.nil?
            if schedule_days.find_by(day_id:self.date.wday).blank?
              hrs = schedule_days.first.total_hours
            else
              hrs = schedule_days.find_by(day_id:self.date.wday).total_hours
            end
            total_day = (hours_worked/hrs.to_f)
            paid_festives -= (total_day > 1)? 1 : total_day
          end
        end if is_holiday
  
        #xx
        #xx if employee.id == 2003 and self.date == '2018-08-22'.to_date
        
        if exists.blank?
          totals[incident_is] = 0 if totals[incident_is].blank?
          totals[incident_is] += 1
          payroll_incident = PayrollIncident.new(enterprise_id:enterprise_id, :employee_id => employee.id, 
            :payroll => payroll.last, :assistances => totals[0], :absences => totals[1], :incapacities => totals[2], 
            :vacations => totals[3], :real_total_hours => previous_time_worked, :total_hours => previous_time_worked,
             :extra_hours => previous_eh, :discounts => (array_incidents.blank?)? "" : array_incidents.to_json.gsub('":','"=>'), 
             :total_discounts => total_discounts, :paid_incapacities => paid_incapacities, :paid_festives => paid_festives)
          payroll_incident.save
        else
  
  
          
          if previous_incident_is != incident_is and self.changes.include?('incident_type_id') and !incident_is.blank?
            totals[previous_incident_is] -=1 if !previous_incident_is.blank? and totals[previous_incident_is] > 0
            totals[incident_is] = 0 if totals[incident_is].blank?
            totals[incident_is] += 1
            if more_than_one
              update_attribute_incident(exists.last, previous_incident_is, totals[previous_incident_is]) if !self.changes['incident_type_id'][0].blank? and !previous_incident_is.blank?
              update_attribute_incident(exists.last, incident_is, totals[incident_is]) #if !previous_incident_is.blank?
            else
              update_attribute_incident(exists, previous_incident_is, totals[previous_incident_is]) if !self.changes['incident_type_id'][0].blank? and !previous_incident_is.blank?
              update_attribute_incident(exists, incident_is, totals[incident_is]) #if !previous_incident_is.blank?
            end
          end
          
          #xx if employee.id == 2003 and self.date == '2018-08-22'.to_date
          if more_than_one
            exists.last.update_attributes(:discounts => (array_incidents.blank?)? "" : array_incidents.to_json.gsub('":','"=>'), 
              :total_discounts => total_discounts, 
              :total_hours => previous_time_worked,
              :real_total_hours => previous_time_worked, 
              :extra_hours => previous_eh, 
              :paid_incapacities => paid_incapacities, 
              :paid_festives => paid_festives)
          else
            exists.update_attributes(:discounts => (array_incidents.blank?)? "" : array_incidents.to_json.gsub('":','"=>'), 
              :total_discounts => total_discounts, 
              :total_hours => previous_time_worked,
              :real_total_hours => previous_time_worked, 
              :extra_hours => previous_eh, 
              :paid_incapacities => paid_incapacities, 
              :paid_festives => paid_festives)
          
          end
  
        end if !incident_is.blank?
      end
    end
  
  
    def go_over_array(array_incidents) 
      if !array_incidents.blank? or array_incidents != ""
        array_incidents.each_with_index do |discount, i|
          if self.date == discount["date"].to_date
            array_incidents.delete_at(i)
          end
        end
      end
  
      array_incidents
    end
  
    def round_number(number)
        return number.to_d.round(2, :truncate).to_f
    end
  
    def update_attribute_incident(payroll_incident, i, total)
      if i == 0
        payroll_incident.update_attribute(:assistances, total)
      elsif i == 1
        #xxs
        payroll_incident.update_attribute(:absences , total)
      elsif i == 2
        payroll_incident.update_attribute(:incapacities , total)
      elsif i == 3
        payroll_incident.update_attribute(:vacations , total)
      end   
    end
  
    
    def date_in_schedule
      if !date.nil? && !employee.nil?
        day_id = date.wday
        day_id = 7 if day_id == 0
        days_ids = employee.schedule.schedule_days.pluck(:day_id)
  
        
        
  
        period = PaymentPeriod.with_date(date, date, employee.company_process_model_id).last
        planned_schedule = employee.plan_employee_schedules.find_by_payment_period_id(period.id) if !period.nil?
        days_ids = planned_schedule.schedule.schedule_days.pluck(:day_id) if !planned_schedule.nil?
        
        if !self.employee.enterprise.company.parameters.where(id: 17).blank?
          if self.employee.enterprise.company.company_parameters.where(:parameter_id => 17).last.value == "0"
           errors.add(:date, I18n.t("date_not_in_schedule")) if days_ids.exclude?(day_id) && (incident_type.letters != "HE" && incident_type.letters != 'I' && incident_type.letters != "DT" && incident_type.letters != "D")
          end
        else
          errors.add(:date, I18n.t("date_not_in_schedule")) if days_ids.exclude?(day_id) && (incident_type.letters != "HE" && incident_type.letters != 'I' && incident_type.letters != "DT" && incident_type.letters != "D")
        end
      end
    end
  
  end
class Permission < ActiveRecord::Base

    belongs_to :role
  
    scope :get_permissions, -> (rol){
      select('controller_name as controller, action_name as action, role_id, ro.name, `condition`')
      .joins('left join roles ro on ro.id = permissions.role_id')
      .where('ro.id = ?', rol)
    }
  
    scope :verify, -> (controller_name, action_name, rol_id){
      select('permissions.id ,controller_name as controller, action_name as action, role_id, ro.name, `condition`')
      .joins('left join roles ro on ro.id = permissions.role_id')
      .where('permissions.controller_name = ? AND permissions.action_name = ? AND role_id = ?', controller_name, action_name, rol_id)
    }
  
   #  permissions by user
   #  scope :get_permissions, -> (username){
      # select('controller_name as controller, action_name as action, id_user, us.username, `condition`')
      # .joins('left join users us on us.id = permissions.id_user')
      # .where('us.username = ?', username)
   #  }
  
   #  scope :verify, -> (controller_name, action_name, id_user){
      # select('permissions.id ,controller_name as controller, action_name as action, id_user, us.username, `condition`')
      # .joins('left join users us on us.id = permissions.id_user')
      # .where('permissions.controller_name = ? AND permissions.action_name = ? AND id_user = ?', controller_name, action_name, id_user)
   #  }
      
  end
  
class Employee < ActiveRecord::Base
  
    attr_accessor :total_isr, :sbc, :subsidization, :isr, 
      :total_hours_sunday, :total_hours_rest_day, :array_extra_hours, 
      :total_loans, :general_payment, :extra_hours_payment, :extra_hours_payment2, :fouls, :concept, 
      :total_extra_hours, :array_extra_hours, :bonuss, :worked_days, :rest_day, :total_worked_days,
      :fixed_discounts, :fixed_bonus_additionals, :cash, :period_salary, :payroll_incidents_discount,
      :contracts,:post_risk_id, :skip_validation,
      :total_payment, :total_vacation ,:total_hours_worked_breaks, :payment_hours_worked_breaks, 
      :imss_result, :imss_owner, :imss_employee, :total_imss, :imss_worked_days, :user_id,:b_assistance,:b_storeroom,:b_punctuality,
      :total_subsidization,:total_daily_fee,:double_extra_time,:triple_extra_time,:total_dobule_worked,:total_triple_worked,:payment_extra_time,
      :perceptions, :perception_other_keys, :deductions_other_keys, :deductions, :discount_incident, :real_salary, :salary_total, :risk_job, 
      :array_extra_hours_double_triple, :taxable_base, :used_tariff, :used_subsidization, :array_information_isr, :incapacity, :festive_day, :payroll_salary_changes, :payroll_fee_changes,
      :change_to_disable,:sunday_bonus, :salary_total_perception, :enterprise_id_register, :salary_payment, :show_perceptions, :show_deductions, 
      :departament_id_register, :job_id_register, :array_information_enterprise, :array_information_employee, :perception_other_keys_total, :deductions_other_keys_total,
      :real_payment, :real_dif_payment, :real_bonus, :real_eh, :real_discounts, :real_total_payment, :real_vacations, :real_settlement, :negative_payment, :imss_real, :imss_paid_modify, :real_infonavit, :real_fonacot, :real_loans, :incidents_discounts,
      :time_discount, :sevent_day, :seven_day_info, :total_seven_day, :christmas_bonus_paymed, :array_info_isr_christmas_bonu, :christmas_bonu_real, :isr_christmas_bonus, :real_saving_fund, :real_saving_fund_pay, 
      :cb_days, :cb_pays, :cb_minus_days, :status_update
  
    #SCOPES
    default_scope { order(:last_name) }
    scope :not_assigneds_except, ->(employee_id) {
      joins("LEFT OUTER JOIN users u ON employees.id = u.employee_id")
      .where("u.id IS NULL OR employees.id = ?", employee_id)
    }
    scope :get_company, ->(company){
      joins("LEFT OUTER JOIN enterprises e ON employees.enterprise_id = e.id")
      .where("e.company_id = ?", company)
    }
  
    scope :all_ajax, ->(term){ 
      where("first_name LIKE ? OR last_name LIKE ?", "%#{term}%","%#{term}%") }
  
   scope :by_enterprise_ajax,->(id,enterprise){ 
      select("id,first_name,last_name,job_id,company_process_model_id,daily_fee,
             employee_number,daily_salary,photo_file_name,enterprise_id")
      .where("enterprise_id=?",enterprise)
      .find(id) }
  
  
  
   scope :date_admission,->(initial_date,final_date){ 
      where("first_admission_date>=?",initial_date)
      .where("first_admission_date<=?",final_date) }
  
    scope :branch_actives,->(enterprise_id, branches){ 
      where('(employees.enterprise_branch_id in (?) OR enterprise_second = ? )', branches, enterprise_id)
    }
  
  
    scope :enterprise_actives,->(enterprise_id, branches){ 
      where('employees.enterprise_branch_id is null OR employees.enterprise_branch_id ="" OR (employees.enterprise_branch_id in (?) OR enterprise_second = ? )', branches, enterprise_id)  
    }
  
  
    scope :date_disable,->(initial_date,final_date){ 
      where("disable_date>=?",initial_date)
      .where("disable_date<=?",final_date) }
  
    scope :is_active_in_payroll, ->(payrolls){
      joins("left join  employee_payrolls ep ON ep.employee_id = employees.id")
      .joins("left join enterprises ent ON employees.enterprise_id = ent.id")
      .where("(ep.payroll_id IN #{payrolls} OR ent.is_subcontractor = 1)")
    }
  
  
  
   scope :dimissions,->{ 
      where("year(disable_date)=year(curdate())") }
    scope :admissions,->{ 
      where("year(admission_date)=year(curdate())") }
  
  
    scope :by_date,->(field_date){ 
      where("month(#{field_date})=month(current_date())")
      .reorder("DAYOFMONTH(#{field_date}) DESC") }
  
    scope :by_sub_department_crew_id, ->(sub_department_crew_id) { where(sub_department_crew_id: sub_department_crew_id) }
    scope :actives, -> date = nil  { where("employees.disable_date IS NULL OR UNIX_TIMESTAMP(employees.disable_date) = 0 OR employees.disable_date > ?", "#{(date.blank?)? Date.today.to_formatted_s(:db) : date.to_formatted_s(:db)}") }
    scope :actives_in_range, -> initial_date = nil, final_date = nil  { where("(employees.disable_date IS NULL OR UNIX_TIMESTAMP(employees.disable_date) = 0 OR employees.disable_date >= ?) && (employees.admission_date IS NULL OR UNIX_TIMESTAMP(employees.admission_date) = 0 OR employees.admission_date <= ?)", initial_date.to_formatted_s(:db), final_date.to_formatted_s(:db)) }
    scope :with_initial_date, -> (final_date) { where("employees.admission_date <= ?", final_date) }
    scope :inactives, -> date = nil { where("employees.disable_date IS NOT NULL AND UNIX_TIMESTAMP(employees.disable_date) > 0 AND employees.disable_date <= ?", "#{(date.blank?)? Date.today.to_formatted_s(:db) : date.to_formatted_s(:db)}") }
    scope :with_schedule_id, ->(schedule_id) { where(schedule_id: schedule_id) }
    scope :with_department_work_id, ->(department_work_id) { where(department_work_id: department_work_id) }
    scope :with_card, -> { where.not(card_front_file_name: nil, card_back_file_name: nil) }
    scope :inactives_last_month, -> {where("employees.disable_date IS NOT NULL and employees.disable_date >= DATE_SUB(CURDATE(),INTERVAL 1 MONTH)")}
    scope :actives_in_range_and_with_perceptions, -> initial_date = nil, final_date = nil, payroll_id  = nil { joins('left join employees_payrolls_perceptions sd on sd.employee_id = employees.id').where("(employees.disable_date IS NULL OR UNIX_TIMESTAMP(employees.disable_date) = 0 OR employees.disable_date >= ?) && (employees.admission_date IS NULL OR UNIX_TIMESTAMP(employees.admission_date) = 0 OR employees.admission_date <= ?) or sd.payroll_id = ? ", initial_date.to_formatted_s(:db), final_date.to_formatted_s(:db), payroll_id).uniq }
    #  scope :outstanding
    scope :vetoed, -> { joins('left join vetoed_types on vetoed_types.id=employees.vetoed_type_id')
      .where("vetoed_types.serious_misconduct= ?",true)}
    #  scope :critical_state
    scope :data_sub_job, -> { select("employees.*,sub_department_crews.name as sub_department_crew_name,jobs.name as job_name")
      .joins("left outer join sub_department_crews on sub_department_crews.id=employees.sub_department_crew_id")
      .joins("left outer join jobs on jobs.id=employees.job_id")
       }
  
    scope :incidents_update, ->(company) { joins("left join enterprises on enterprises.id=employees.enterprise_id")
      .where("enterprises.company_id= ?",company)
    }
  
    scope :enterprise_active, -> { joins('left join enterprises on enterprises.id=employees.enterprise_id')
      .where('enterprises.is_subcontractor = 0')}
  
     scope :employer_registration_inactives, -> (id_enterprise,string_department,flag){
     select("employees.id,employees.employee_number,employees.last_name,employees.first_name,employees.disable_date,employees.salary_type,employees.admission_date,employees.date_imss_salary,employees.imss_salary,er.employer_registration")
     .joins("left join enterprise_employer_registrations er on employees.enterprise_employer_registration_id = er.id")
     .where("employees.enterprise_id = #{id_enterprise} #{string_department} #{flag}") 
     .inactives_last_month
     }
  
     scope :employer_registration_actives, -> (id_enterprise,string_department,flag){
     select("employees.id,employees.employee_number,employees.last_name,employees.first_name,employees.disable_date,employees.admission_date,employees.date_imss_salary,employees.imss_salary,employees.salary_type,er.employer_registration")
     .joins("left join enterprise_employer_registrations er on employees.enterprise_employer_registration_id = er.id")
     .where("employees.enterprise_id = #{id_enterprise} #{string_department} #{flag}") 
     .actives
     }
  
     scope :get_incidents_employee,->(query,group,sql){
      select("ity.letters,ity.name, count(ity.letters) as total_incident,ity.color #{sql}")
      .joins("inner join incidents inc on inc.employee_id = employees.id")
      .joins("inner join incident_types ity on ity.id = inc.incident_type_id")
      .where("#{query} ")
      .where("concat(day(inc.date),'/',month(inc.date)) not in (select concat(day(date),'/',month(date)) from holidays)")
      .where("ity.letters in ('M','FL','F','RE','I','R','E','PJ','PT', 'CA','FI')")
      .group("inc.incident_type_id #{group}")
     }
  
      #ASSOCIATIONS
  belongs_to :enterprise
  belongs_to :enterprise_employer_registration
  belongs_to :enterprise_branch

  belongs_to :job
  belongs_to :department_work
  belongs_to :sub_department_crew
  belongs_to :subdepartment
  #new subdepartments
  belongs_to :enterprise_taxed

  has_one :taxes_data_employee

  belongs_to :concept
  belongs_to :schedule
  belongs_to :bank
  belongs_to :company_process_model
  belongs_to :city
  belongs_to :state
  belongs_to :state
  belongs_to :country
  belongs_to :regimen_type
  belongs_to :contract
  belongs_to :level_employee
  belongs_to :vetoed_type
  
  has_many :profit_centers
  has_many :employee_uploads
  has_many :settlements

  has_many :employee_catalog_concepts, :dependent => :destroy
  has_many :employee_families
  has_many :employee_additional_informations
  has_many :salary_changes

  has_many :employee_fixed_discounts, :dependent => :destroy
  has_many :employee_fixed_bonus_additionals, :dependent => :destroy
  has_many :employee_policies_bonus, :dependent => :destroy
  has_many :settlement
  has_many :fixed_bonus_additional , :through => :employee_fixed_bonus_additionals
  has_many :policies_bonu , :through => :employee_policies_bonus

  has_many :employees_document_structures, :dependent => :destroy
  has_many :document_structures, :through => :employees_document_structures
  has_many :schedule_days, :through => :schedule
  has_many :reading_assistances
  has_many :incidents, :dependent => :destroy
  has_many :vacations, :dependent => :destroy
  has_many :notifications, :dependent => :destroy
  has_many :loans, :dependent => :destroy
  has_many :employee_payrolls
  has_many :employee_christmas_bonus
  has_many :employees_payrolls_deductions
  has_many :employees_payrolls_perceptions
  has_many :employees_bonus_discounts
  has_many :fonacot_loans
  has_many :infonavit_credits
  has_many :plan_employee_schedules
  
  has_many :employee_incapacities
  has_many :payment_periods, :through => :plan_employee_schedules
  has_many :payroll_incidents

  has_many :employee_payroll_payroll_types, :through => :employee_payrolls
  has_many :labour_paths
  has_one :user
  has_many :catalog_concepts, :through => :employee_catalog_concepts 
  has_many :incapacities

  has_attached_file :photo,
    :styles => { :original => "x300",
    :mobile => "x240" },
    :default_url => "no-image.png"

  
  
    def exist_imss_date
      self.date_imss_salary = self.admission_date if self.date_imss_salary.blank?
    end



    def photo_url
      if photo_file_name.blank? 
        "/assets/no-image.png"
      else
        "/system/employees/photos/000/"+get_number(id.to_i)+"/original/#{photo_file_name}"
      end
    end

    def has_enrolled
       !(photo_file_name.blank? || signature_file_name.blank?)
    end

    def get_number id
        value = "%06d" % id.to_s
        value = value.insert(3, '/')
        url = ""
        value.split('').each do |v| 
          url += v
        end
        url
    end
  
  
  
    def employee_changes
        if self.changed?
          self.changed.each do |field_changed|
            if %w{first_name last_name sex rfc curp social_insurance 
                  imss_salary admission_date first_admission_date disable_date 
                  vetoed_type_id state_id salary_type date_imss_salary medical_unit 
                  employee_imss_type imss_working_day infonavit infonavit_number zip birth_date state_id}.include?(field_changed)
                Employee.update(self.id,:idse_flag => 0,:sua_flag =>0,:sua_flag_affiliate => 0)
            end
          end
  
          #Tony no borres please
          if self.changed.include?("department_work_id")
            date = Time.now
            pp =  PaymentPeriod.where("final_date <= ?", date).last
            payroll_last_closed = Payroll.joins('left join payment_periods pp ON pp.id = payrolls.payment_period_id').where('payrolls.enterprise_id = ? AND payrolls.closed = ? AND payrolls.payroll_description_id = 1 AND pp.company_process_model_id = ?',  self.enterprise.id, true, self.company_process_model_id).last
            payrolls_opened = Payroll.where('enterprise_id = ? AND closed = ? AND payroll_description_id = 1 AND payment_period_id <= ? AND payment_period_id >= ?',  self.enterprise.id, false, pp,payroll_last_closed.payment_period_id) if !payroll_last_closed.blank?
            payrolls_opened.each do  |po|
              update_department = TaxesDataEmployee.find_by_payroll_id_and_employee_id(po.id, self.id)
              update_department.update(department_work_id:self.department_work_id) if !update_department.blank?
            end if !payrolls_opened.blank?
          end
        end
    end
  
    def models_id_exists
      models = [
        "Enterprise",
        "EnterpriseEmployerRegistration",
        "Job",
        "Subdepartment",
        "Schedule",
        "CompanyProcessModel",
        "LevelEmployee"
              ]
        attributes_model = [
          "enterprise_id",
          "enterprise_employer_registration_id",
          "job_id",
          "subdepartment_id",
          "schedule_id",
          "company_process_model_id",
          "level_employee_id"
        ]
  
    models.each_with_index do |model, index|
          begin
            Object.const_get(model).find(self.send(attributes_model[index]))
          rescue ActiveRecord::RecordNotFound
            model_translate = attributes_model[index].chomp("_id")
            model = I18n.t("activerecord.models.#{model_translate}")
            errors.add(model, " debe de exitir")
            false
          end
      end
    end
  
    
    def date_imss_salary_employee
        errors.add(:date_imss_salary, ' debe de asignar una fecha para el cambio de SDI.') if !date_imss_salary_changed? && !@new_record
    end
  
  
    def new_date_valid?
      if !self.disable_date.nil?
        errors.add(:disable_date, ' no puede ser menor a la fecha del último ingreso.') if (self.admission_date > self.disable_date)
      end
    end
    #METHODSss
  
    def get_deductions(employee, payroll)
     payroll_deduction = EmployeesPayrollsDeduction.where(payroll_id: payroll)
      .where(employee_id: employee).where.not(key_deduction: "006")
      #.where.not(key_deduction: "001")
      #.where.not(key_deduction: "002")
     return payroll_deduction
  end
  
    def get_perception(employee, payroll, concepts)  
     payroll_perception = EmployeesPayrollsPerception
      .where(payroll_id: payroll).where(employee_id: employee)
      .where.not(key_perception: "014")
      .where.not(key_perception: "003").where.not(key_perception: "019")
      .where.not(key_perception: "010").where.not(key_perception: "049")
      .where.not(key_perception: "029")
      .where.not(code: concepts)
      .where('is_seven_day <> ? or is_seven_day is null', true)
     return payroll_perception
  end
  
   def get_total_perception(employee_id, payroll_id, concepts)
     get_total_perception =  EmployeesPayrollsPerception.where(employee_id: employee_id).where(payroll_id: payroll_id).where.not(key_perception: "014").where.not(code: concepts).where.not(key_perception: "003").where.not(key_perception: "019").where.not(key_perception: "010").where.not(key_perception: "049").where.not(key_perception: "029").where.not(key_perception: "000").sum(:total)
     get_total_perception +=  EmployeeFixedBonusAdditional.where(employee_id: employee_id).sum(:amount)
      return get_total_perception
    end
  
    def get_total_perception_clouse(employee_id, payroll_id, concepts)
      get_total_perception =  EmployeesPayrollsPerception.where(employee_id: employee_id).where(payroll_id: payroll_id).where.not(key_perception: "014").where.not(code: concepts).where.not(key_perception: "003").where.not(key_perception: "019").where.not(key_perception: "010").where.not(key_perception: "049").where.not(key_perception: "029").where.not(key_perception: "000").where('is_seven_day <> ? or is_seven_day is null', true).sum(:total)
      return get_total_perception
    end
     def get_total_deduction(employee_id, payroll_id)
     get_total_deduction =  EmployeesPayrollsDeduction.where(employee_id: employee_id).where(payroll_id: payroll_id).where.not(key_deduction: "001").where.not(key_deduction: "002").where.not(key_deduction: "006").sum(:total)
     get_total_deduction +=  EmployeeFixedDiscount.where(employee_id: employee_id).sum(:amount)
      return get_total_deduction
    end
  
    def get_total_deduction_clouse(employee_id, payroll_id)
      ccd_concepts = CatalogConcept.where(:is_ccd => true)
      get_total_deduction =  EmployeesPayrollsDeduction.where(employee_id: employee_id).where(payroll_id: payroll_id).where.not(key_deduction: "001").where('(key_deduction not in("002")) or (key_deduction in("002") and  code  in(?))', ccd_concepts.pluck(:key_concept)).where.not(key_deduction: "006").sum(:total)
      return get_total_deduction
    end
  
   def get_bonus_punctuality(employee, payroll)
     bonu_punctuality = EmployeesPayrollsPerception.where(payroll_id: payroll).where(employee_id: employee).where(code: "010").first
     return bonu_punctuality
  end
  
  def get_bonus_assistance(employee, payroll)
     bonu_assistance = EmployeesPayrollsPerception.where(payroll_id: payroll).where(employee_id: employee).where(code: "049").first
     return bonu_assistance
  end
  
  def get_bonus_pantry(employee, payroll)
     bonu_pantry = EmployeesPayrollsPerception.where(payroll_id: payroll).where(employee_id: employee).where(code: "029").first
     return bonu_pantry
  end
  
    def is_active(id)
      active_employee = Employee.actives.where(id: id)
      return active_employee.count
    end
  
    def is_new(yearSelect)
      employeeNew = 0
      if admission_date.strftime("%Y") == yearSelect
       employeeNew = 1
     end
     return employeeNew
    end
  
  def get_incidents
      incidents_from_admission = incidents.where("date <= curdate()").count
      incidents_data_attributes_absences = incidents.absences.count
      incidents_data_attributes_absencesYear = incidents.absences_year.count
  
      return incidents_data_attributes_absences, incidents_data_attributes_absencesYear
    end
  
  
  
  
    def get_real_christmas_bonus_days(yearSelect, date, calculate_absence, calculate_incapacities, by_ids, ids)
      disable_date = date
      year_days = 365
      current_year = (yearSelect.blank?)? Date.today.strftime("%Y") : yearSelect
      
  
  
      if !disable_date.blank?
        year_days = (disable_date.to_date + 1) - Date.parse(current_year + '/01/01')
      else
        year_days = (Date.parse(current_year + '/12/31') - Date.parse(current_year + '/01/01') + 1)
        disable_date =   (current_year + '/12/31').to_date
      end
  
  
  
      aux_final_date = Date.current
      aux_admission_year = admission_date
      if self.employee_uploads.blank?
        admission_year = admission_date.strftime("%Y")
      else
        if yearSelect.to_i == Date.current.year
          aux_admission_year_d = self.employee_uploads.where('admission_date <= ? AND (disable_date is null or disable_date >= ?)', Date.current, Date.current)
          if !aux_admission_year_d.blank?
            aux_admission_year =  aux_admission_year_d.last.admission_date
          end
  
          admission_year = aux_admission_year.strftime("%Y")
        else
          aux_final_date = Date.parse(current_year + '/12/31')
          aux_admission_year_d = self.employee_uploads.where('admission_date <= ? AND (disable_date is null or disable_date >= ?)', aux_final_date, aux_final_date)
          if !aux_admission_year_d.blank?
            aux_admission_year =  aux_admission_year_d.last.admission_date
          end
  
          admission_year = aux_admission_year.strftime("%Y")
        end
      end
      discount_days_for_incidents , worked_days_in_year, chritmas_bonus_days = 0.0, 0.0, 0.0
  
  
  
      if active
  
        if by_ids
          if ids.include?('12')
            aux_ids = ids.delete('12')
            discount_12 = self.incidents.where(incident_type_id: 12, :inability_type_id => 2).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            discount_others = self.incidents.where(incident_type_id: aux_ids).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            
  
            if !disable_date.blank?
              discount_12 = discount_12.where("date <= ?", disable_date)
              discount_others = discount_others.where("date <= ?", disable_date)
            end
  
            discount_days_for_incidents = discount_12.count + discount_others.count
          else
            discount_others = self.incidents.where(incident_type_id: ids).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            discount_days_for_incidents = discount_others.count
          end
        else
          discount_incident_14 = discount_incident_24 = discount_incident_22 = discount_incident_12 = []
          if calculate_incapacities
            discount_incident_12 = self.incidents.where(incident_type_id: 12, :inability_type_id => 2).where("year(date) = ?", current_year)
          end
  
          if calculate_absence
            discount_incident_14 = self.incidents.where(incident_type_id: 14).where("year(date) = ?", current_year).where('date < ?', aux_final_date)
            discount_incident_24 = self.incidents.where(incident_type_id: 24).where("year(date) = ?", current_year).where('date < ?', aux_final_date)
            discount_incident_22 = self.incidents.where(incident_type_id: 22).where("year(date) = ?", current_year).where('date < ?', aux_final_date)
          end
  
          if disable_date.blank?
            discount_incident_12 = discount_incident_12.where("date <= ?", disable_date.to_date) if calculate_incapacities
            if calculate_absence
              discount_incident_14 = discount_incident_14.where("date <= ?", disable_date.to_date)
              discount_incident_24 = discount_incident_24.where("date <= ?", disable_date.to_date)
              discount_incident_22 = discount_incident_22.where("date <= ?", disable_date.to_date)
            end
          end
  
          discount_days_for_incidents = discount_incident_12.count + discount_incident_24.count + discount_incident_14.count + discount_incident_22.count
        end
  
        if self.incidents.where("incident_type_id not in (14, 24, 22) AND (inability_type_id is null or inability_type_id != 2)").where("year(date) = ?", current_year).count != 0
          if admission_year == current_year
            if disable_date.blank?
              end_date  = (current_year == Date.today.strftime("%Y"))? Date.today : (Date.parse(current_year + '/12/31'))
              worked_days_in_year = (end_date - admission_date + 1) - discount_days_for_incidents
            else
              worked_days_in_year = (disable_date.to_date - admission_date.to_date + 1) - discount_days_for_incidents
            end
          elsif admission_year < current_year
              worked_days_in_year = year_days - discount_days_for_incidents
          end
        else
  
          worked_days_in_year = (disable_date - aux_admission_year).to_i + 1 - discount_days_for_incidents
          #discount_days_for_incidents = 0
        end
      else
        #uploads = self.employee_uploads.where('(admission_date >= "'+yearSelect+'-01-01" and admission_date <=  "'+yearSelect+'-12-31" and disable_date is null) or disable_date >=  "'+yearSelect+'-01-01" ))')
  
  
        if by_ids
          if ids.include?('12')
            aux_ids = ids.delete('12')
            discount_12 = self.incidents.where(incident_type_id: 12, :inability_type_id => 2).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            discount_others = self.incidents.where(incident_type_id: aux_ids).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            
  
            if !disable_date.blank?
              discount_12 = discount_12.where("date <= ?", disable_date)
              discount_others = discount_others.where("date <= ?", disable_date)
            end
  
  
            discount_days_for_incidents = discount_12.count + discount_others.count
          else
            discount_others = self.incidents.where(incident_type_id: ids).where('date >= ?', self.admission_date).where("year(date) = ?", current_year)
            discount_days_for_incidents = discount_others.count
          end
        else
  
          discount_incident_14 = discount_incident_24 = discount_incident_22 = discount_incident_12 = []
          if calculate_incapacities
            discount_incident_12 = self.incidents.where(incident_type_id: 12, :inability_type_id => 2).where("year(date) = ?", current_year)
          end
  
          
  
          if calculate_absence
            discount_incident_14 = self.incidents.where(incident_type_id: 14).where("year(date) = ?", current_year)
            discount_incident_24 = self.incidents.where(incident_type_id: 24).where("year(date) = ?", current_year)
            discount_incident_22 = self.incidents.where(incident_type_id: 22).where("year(date) = ?", current_year)
          end
  
          if disable_date.blank?
            discount_incident_12 = discount_incident_12.where("date <= ?", disable_date.to_date) if calculate_incapacities
            if calculate_absence
              discount_incident_14 = discount_incident_14.where("date <= ?", disable_date.to_date)
              discount_incident_24 = discount_incident_24.where("date <= ?", disable_date.to_date)
              discount_incident_22 = discount_incident_22.where("date <= ?", disable_date.to_date)
            end
          end
  
          discount_days_for_incidents = discount_incident_12.count + discount_incident_24.count + discount_incident_14.count + discount_incident_22.count
        end
        if self.incidents.where("incident_type_id not in (14, 24, 22) AND (inability_type_id is null or inability_type_id != 2)").where("year(date) = ?", current_year).count != 0
          
          if admission_year == current_year
            if disable_date.blank?
              end_date  = (current_year == Date.today.strftime("%Y"))? Date.today : (Date.parse(current_year + '/12/31'))
              worked_days_in_year = (end_date - admission_date + 1) - discount_days_for_incidents
            else
              worked_days_in_year = (disable_date.to_date - admission_date.to_date + 1) - discount_days_for_incidents
            end
          elsif admission_year < current_year
              worked_days_in_year = year_days - discount_days_for_incidents
          end
        else
          worked_days_in_year = (Date.current - aux_admission_year).to_i + 1 - discount_days_for_incidents      
        end
  
      end
  
      
      
      if worked_days_in_year > 0
        chritmas_bonus_days = (worked_days_in_year.to_f * level_employee.days_chritsmas_bonus.to_f) / 365#371#365
      end
      
      return discount_days_for_incidents, chritmas_bonus_days, worked_days_in_year, aux_admission_year
    end
      
    def christmas_bonus_information(yearSelect, date, calculate_absence, calculate_incapacities, discount_by_id ,ids_discount)
      return get_real_christmas_bonus_days(yearSelect, date, calculate_absence, calculate_incapacities, discount_by_id, ids_discount)
    end
  
  
    def get_christmas_bonus_days_today
      return get_real_christmas_bonus_days(nil, nil, true, true, false, nil)[1]
    end
  
    def christmas_bonu(yearSelect)
       año_Actual = Date.today.strftime("%Y")
  
       #registro = ChristmasBonu.where(employee_id: id).where(year: yearSelect)
  
        #if registro.blank?
          dias_aguinaldo = 0
          sueldo_Diario = 0
        #end
   
        menos_Dias_Aguinaldo14 = Incident.where(employee_id: id).where(incident_type_id: 14).where("year(date) = ?", yearSelect).count
        menos_Dias_Aguinaldo22 = Incident.where(employee_id: id).where(incident_type_id: 22).where("year(date) = ?", yearSelect).count
        menosDiasAguinaldoTotal = menos_Dias_Aguinaldo14 + menos_Dias_Aguinaldo22
  
        if admission_date.nil? or yearSelect > año_Actual
                dias_Pagar = "Empleado sin fecha de ingreso"
                aguinaldo = ""
        elsif (!disable_date.nil?) and (admission_date.strftime("%Y") == yearSelect) and admission_date.strftime("%Y") == año_Actual
                dias_Pagar = (disable_date - admission_date + 1) - menosDiasAguinaldoTotal
                aguinaldo = ((dias_aguinaldo.to_f/365) * dias_Pagar)*sueldo_Diario
        elsif admission_date.strftime("%Y") == yearSelect
                dias_Pagar = (Date.parse(yearSelect + '/12/31') - admission_date + 1) - menosDiasAguinaldoTotal
                aguinaldo = ((dias_aguinaldo.to_f/365) * dias_Pagar)*sueldo_Diario
        elsif (!disable_date.nil?) and (admission_date.strftime("%Y") != yearSelect)
                  dias_Pagar = 0
                  aguinaldo = 0
        else 
                dias_Pagar = (Date.parse(yearSelect + '/12/31') - Date.parse(yearSelect + '/01/01') + 1) - menosDiasAguinaldoTotal
                aguinaldo = ((dias_aguinaldo.to_f/365) * dias_Pagar)*sueldo_Diario
        end 
  
        return menosDiasAguinaldoTotal, dias_Pagar, aguinaldo, dias_aguinaldo
    end
  
  
    def age
      if birth_date.blank?
        nil
      else
        ((Date.today - birth_date) / 365).to_i
      end
    end
    
    def enterprise_name
      enterprise.to_s
    end
    
    def save_labour_path
      self.changes.each do |key, value|
        if %w{id created_at updated_at}.exclude?(key)
          LabourPath.create(employee_id: id, user_id: user_id, field: key, value: value[1])
        end
      end
    end
  
    def save_salary_changes
      params =  false
      array_changes = Hash.new 
      %w{imss_salary date_imss_salary daily_fee daily_salary}.each do |key, value|
        
        if self.changes.include?(key)
          params = true
          array_changes = array_changes.merge!(key.to_sym => self.changes[key][1])
        end
  
      end
      
      if params
        array_changes = array_changes.merge!(:date => Date.today)
        exists = SalaryChange.where(:employee_id => self.id, :change_date => array_changes[:date])
        if exists.blank?
          array_changes[:date_imss_salary] = self.date_imss_salary if array_changes[:date_imss_salary].blank?
          array_changes[:date_imss_salary] = Time.now.to_date if array_changes[:date_imss_salary].blank?
  
          SalaryChange.create(user_id: user_id, job_id: self.job_id, :employee_id => self.id , :change_date => array_changes[:date], :imss_date =>array_changes[:date_imss_salary], :imss_salary =>array_changes[:imss_salary] , :daily_fee=> array_changes[:daily_fee], :real_salary => array_changes[:daily_salary])
        else        
          if (exists.last.imss_salary.blank? and  self.date_imss_salary == exists.last.imss_date) or (Behavor.first.manage == 1 and exists.last.real_salary.blank?)
            date =  (array_changes[:date_imss_salary].blank?)? exists.last.imss_date : array_changes[:date_imss_salary]
            exists.last.update(user_id: user_id, job_id: self.job_id, :imss_date => date)
            
            if !(array_changes[:daily_salary]).blank?
              exists.last.update( :real_salary => array_changes[:daily_salary])
            end
            if !(array_changes[:imss_salary]).blank?
              exists.last.update(:imss_salary =>array_changes[:imss_salary])
            end
            if !(array_changes[:daily_fee]).blank?
               exists.last.update(:daily_fee => array_changes[:daily_fee])
            end
          elsif array_changes[:daily_fee].blank? and array_changes[:imss_salary].blank? and array_changes[:real_salary].blank?
            exists.last.update(:imss_date => array_changes[:date_imss_salary]) if !array_changes[:date_imss_salary].blank?
          else
            array_changes[:date_imss_salary] = self.date_imss_salary if array_changes[:date_imss_salary].blank?
            SalaryChange.create(user_id: user_id, job_id: self.job_id, :employee_id => self.id , :change_date => array_changes[:date], :imss_date =>array_changes[:date_imss_salary], :imss_salary =>array_changes[:imss_salary] , :daily_fee=> array_changes[:daily_fee], :real_salary => array_changes[:daily_salary])
          end
          
        end
      end
    end
  
    def save_sign_in_out
      
      if self.changes.include?('admission_date') and self.changes.include?('disable_date')
        if !self.employee_uploads.blank? 
          self.employee_uploads.last.update_attribute(:disable_date , self.disable_date)
          self.employee_uploads.last.update_attribute(:admission_date , self.admission_date)
        else
          self.employee_uploads.new(:employee_id => self.id, :enterprise_id => self.enterprise_id, :enterprise_branch_id => self.enterprise_branch_id, :admission_date => self.admission_date, :disable_date => self.disable_date)
        end      
      elsif self.changes.include?('admission_date')
        if self.employee_uploads.blank? 
          self.employee_uploads.new(:employee_id => self.id, :enterprise_id => self.enterprise_id, :enterprise_branch_id => self.enterprise_branch_id, :admission_date => self.admission_date)
        else
          if !self.employee_uploads.blank?
            uploads = self.employee_uploads.where(:disable_date => nil)
            if !uploads.blank?
              uploads.last.update_attribute(:admission_date, self.changes[:admission_date][1])
            else
              self.employee_uploads.new(:employee_id => self.id, :enterprise_id => self.enterprise_id, :enterprise_branch_id => self.enterprise_branch_id, :admission_date => self.admission_date)
            end
          else
            self.employee_uploads.new(:employee_id => self.id, :enterprise_id => self.enterprise_id, :enterprise_branch_id => self.enterprise_branch_id, :admission_date => self.admission_date)
          end
        end
        
      elsif self.changes.include?('disable_date')
        if !self.disable_date.blank?
          days_amount =  (self.disable_date - self.admission_date).to_i + 1
          
          if !self.employee_uploads.blank? 
            uploads = self.employee_uploads.where(:disable_date => nil)
            if !uploads.blank?
              uploads.last.update_attribute(:disable_date, self.disable_date)
              uploads.last.update_attribute(:days_active, days_amount)
            else
              self.employee_uploads.last.update_attribute(:disable_date , self.disable_date)
              self.employee_uploads.last.update_attribute(:days_active , days_amount)
            end
          else
            self.employee_uploads.new(:employee_id => self.id, :enterprise_id => self.enterprise_id, :enterprise_branch_id => self.enterprise_branch_id, :admission_date => self.admission_date, :disable_date => self.disable_date, :days_active => days_amount)
          end   
        else   
          if !self.employee_uploads.blank? 
            self.employee_uploads.last.update_attribute(:disable_date, nil)
            self.employee_uploads.last.update_attribute(:days_active, nil)
          end
        end
      elsif self.changes.include?('enterprise_branch_id')
        self.employee_uploads.last.update_attribute(:enterprise_branch_id, self.enterprise_branch_id)
      end
  
      
  
      
      
    end
   
    
    def check_params
      if self.bonus.nil?
        self.bonus = 0.0
      end
      if self.bonus_assistance.nil?
        self.bonus = 0.0
      end
      if self.bonus_storeroom.nil?
        self.bonus = 0.0
      end
      
  
    end
    
    def set_admission_date
      self.first_admission_date = self.admission_date if !self.admission_date.blank?
    end
    
    def active(date = nil)
      disable_date.blank? || disable_date>((date.nil?)? Date.today : date)
      
    end
    
    def get_new_document_structures(current_user)
      if new_record?
        current_user.company.document_structures
      else
        department_work.company_branch.company.document_structures.select("id")
        .where("id NOT IN (SELECT document_structure_id FROM employees_document_structures WHERE employee_id = ?)", id)
      end
    end
    
    def get_document_structures_left
      department_work.company_branch.company.document_structures.select("document_structures.id AS document_structure_id, 
        name,
        eds.id AS employees_document_structure_id,
        document_file_name")
      .join("LEFT JOIN (SELECT * FROM employees_document_structures WHERE employee_id = #{((new_record?)? "NULL" : id)}) AS eds 
        ON document_structures.id = eds.document_structure_id")
    end
    
    def to_s
      fullname
    end
    
    def select_display
      fullname_reverse
    end
    
    def number_and_name
      "#{employee_number} #{fullname_reverse}"
    end
    
    def fullname
      "#{first_name} #{last_name}"
    end
    
    def fullname_reverse
      "#{last_name} #{first_name}"
    end
    
    def job_name
      job.name if !job.nil?
    end
    
    def sub_department_crew_name
      sub_department_crew.name if !sub_department_crew.nil?
    end
    
    def sub_department_crew_id
      sub_department_crew.id if !sub_department_crew.nil?
    end
    
    
    def signature_url
      signature.exists? ? signature.url.sub(/\?\d+$/, ""): "/assets/no-image.png"
    end
    
    def card_front_url
      card_front.exists? ? card_front.url.sub(/\?\d+$/, ""): "/assets/no-image.png"
    end
    
    def card_back_url
      card_back.exists? ? card_back.url.sub(/\?\d+$/, ""): "/assets/no-image.png"
    end
    
    def has_photo?
      photo.exists?
    end
    
    def has_signature?
      signature.exists?
    end
    
    def is_enrolled?
      has_photo? && has_signature?
    end
    
   def calculate_antiquity(start,end_lab)
  
      start_time = Time.new(start.year,start.month,start.day)
      end_time = Time.new(end_lab.year,end_lab.month,end_lab.day)
  
      periodWorked = TimeDifference.between(start_time, end_time).in_weeks.to_i
  
      return periodWorked
    end
  
  
    def employee_antiguity(disable_date)
        disable_date = (disable_date.blank?)? Date.today : disable_date
        
        start_time = Time.new(admission_date.year,admission_date.month,admission_date.day)
        end_time = Time.new(disable_date.year,disable_date.month,disable_date.day)
      
        periodWorked = TimeDifference.between(start_time, end_time).in_general
  
        return periodWorked
    end
  
    
    def get_days_enjoyment(disable_date)
      disable_date = (disable_date.blank?)? Date.today : disable_date
      periodWorked = employee_antiguity(disable_date)
  
      total_days_free = (disable_date - admission_date.to_date).to_i % 365 + 1
  
      seniority_employee_year = periodWorked[:years]
  
      days = VacationParam.select("SUM(days) as days_law").where("years <= ?", seniority_employee_year)
      days_extra_level = level_employee.days_extra_vacations * seniority_employee_year
      days_enjoyment_total = days[0]["days_law"].to_i + days_extra_level + proportional_days(seniority_employee_year + 1, total_days_free)
  
      
      max_vacation = department_work.company_branch.company.company_parameters.find_by_parameter_id(6).value.to_i
  
      if total_days_free <= max_vacation and max_vacation != 0
        days_disponible_total = proportional_days(seniority_employee_year + 1, total_days_free)
      else
        days_disponible = VacationParam.select("SUM(days) as days_disponible").where("years = ?", seniority_employee_year)
  
        days_disponible = (days_disponible.blank?)? 0 : days_disponible[0]["days_disponible"].to_i
  
        if !self.vacations.where(:vacation_param_id => seniority_employee_year).blank?
          days_disponible -= self.vacations.where(:vacation_param_id => seniority_employee_year).sum(:requested_days).to_i
        end
        days_disponible_total = days_disponible + days_extra_level + proportional_days(seniority_employee_year + 1, total_days_free)
      end 
  
      return days_enjoyment_total, days_disponible_total
    end
  
    def proportional_days(year, days)
        days_next_period = VacationParam.select("SUM(days) as days_next_period").where("years = ?", year)
        days_extra_level_next_period  = level_employee.days_extra_vacations * year
        days_disponible_total_next_period = days_next_period[0]["days_next_period"].to_i + days_extra_level_next_period
  
        proportional_days_total = (days * days_disponible_total_next_period).to_f / (365).to_f
  
        return proportional_days_total
    end
  
  
    def get_days_busy(disable_date, year)
      disable_date = (disable_date.blank?)? Date.today : disable_date
      if year.blank?
        periodWorked = employee_antiguity(disable_date)
        year = periodWorked[:years]
      end
  
      vacation_params = VacationParam.find_by_years(year)
  
      if vacation_params.nil?
        if periodWorked[:years] == 0
          days = vacations.where('vacation_statu_id=5').where("vacation_param_id = ?", 1).sum(:requested_days)
        else
          days = 0
        end
      else
        days = vacations.where('vacation_statu_id=5').where("vacation_param_id = ?", vacation_params.id).sum(:requested_days)
      end
  
      return days
    end  
  
    def calculate_daily_fee(employee)
      daily_fee = calculate_daily_fee_without_final_date(employee)
      return daily_fee
    end
     
    
  
  
    def get_employee_info(payroll_id)
      data_attributes = attributes
  
      its_schedule = schedule
  
      if payroll_id != 0
        planned_schedule = Payroll.where(id:payroll_id).last.payment_period
        if !planned_schedule.blank?
          planned_schedule = plan_employee_schedules.where(:payment_period_id => planned_schedule.id).last
          if !planned_schedule.blank?
            its_schedule =  planned_schedule.schedule
          end
        end
      else
        its_schedule = schedule
      end
  
  
      path = self.enterprise.logo_enterprise_url
  
      data_attributes["fullname"] = fullname
      data_attributes["enterprise_logo"] = path
      data_attributes["enterprise_name"] = enterprise.select_display
      data_attributes["department_work"] = department_work.select_display if !department_work.nil?
      data_attributes["profit_name"] = department_work.profit_name if !department_work.nil?
      data_attributes["sub_department_crew"] = sub_department_crew.select_display if !sub_department_crew.nil?
      data_attributes["job"] = job.select_display
      data_attributes["photo_url"] = photo_url
      
      time_now = department_work.time_zone_time_now
      schedule_day_attributes = get_current_schedule_day(time_now)
      causes = []
      
      if !schedule_day_attributes.nil?
        data_attributes["reading_assistances"] = reading_assistances
        .where("reading_assistances.date = ? AND reading_assistances.schedule_day_id = ?", schedule_day_attributes["date"], schedule_day_attributes["id"]).map do |r|
          cause = r.cause
          causes << cause
        
          {
            cause: cause,
            registration_time: r.registration_time.to_s
          }
        end
      else
        data_attributes["reading_assistances"] = []
      end
      
      data_attributes["reading_assistances"] += (%w{entry meal_start meal_end departure extra_entry extra_departure} - causes).map { |c| { cause: c, registration_time: nil } }
      #data_attributes["photo"] = photo(:mobile)
      
      limit = 3;
      
      data_attributes["payment_periods"] = PaymentPeriod.where(company_process_model_id: company_process_model_id)
      .where("curdate() <= final_date")
      .limit(limit)
  
      data_attributes["payment_periods_opened"] =  PaymentPeriod.where(:company_process_model => self.company_process_model).where('initial_date >= ?', Date.today - 2.month).limit(15)
  
      ##data_attributes["payment_periods_opened"] = PaymentPeriod.where(:id => Payroll.last_payrolls(self.enterprise_id).pluck(:payment_period_id)).limit(10)
      
      days_after_last_payment = 30;
      data_attributes["days_after_last_payment"] = days_after_last_payment
      
      data_attributes
    end
  
  
    def get_data_attributes
      get_employee_info(0)
    end
  
    def get_data_attributes_payroll(payroll_id)
      get_employee_info(payroll_id)
    end
    
    #Periodo_actual_or_check #Dias tomados en el periodo, #días_a_disfrutar
    def get_data_attributes_vacations(year)    
      days_busy = get_days_busy(nil, year)    
      days_enjoyment, days_disponible_total = get_days_enjoyment(nil)   
      start_time = Time.new(admission_date.year,admission_date.month,admission_date.day)
      end_time = Time.new(Time.current.year,Time.current.month,Time.current.day)
      if !year.blank?
        periodWorked = {:years=>year, :months=>0, :weeks=>0, :days=>0, :hours=>0, :minutes=>0, :seconds=>0}  
      else
        periodWorked = TimeDifference.between(start_time, end_time).in_general
      end
      return periodWorked, days_busy, days_enjoyment
    end
  
    def get_days_current_year
      days_enjoyment, days_disponible_total = get_days_enjoyment(nil)
      return days_disponible_total
    end
  
    
  
    def get_days_enjoy(year)
      if year > 0
      vacation_param = VacationParam.where("years = ?", year).first
  
      days = vacations.where(vacation_statu_id: 5).where(vacation_param_id: vacation_param.id).sum(:requested_days)
      else
        days = 0
      end
      return days
    end
  
    def get_loans_data_attributes
      loans_data_attributes = {}
      loans_data_attributes["loans_total"] = loans.count
      loans_data_attributes["loans_amount_borrowed_sum"] = number_to_currency(loans.pluck(:amount_borrowed).inject(:+) || 0)
      loans_data_attributes["loans_amount_paid_sum"] = number_to_currency(loans.pluck(:amount_paid).inject(:+) || 0)
      loans_data_attributes
    end
    
    def get_incidents_data_attributes
      
      incidents_data_attributes = {}
      incidents_from_admission = incidents.where("date <= curdate()").count
      incidents_data_attributes["absences_total"] = incidents.absences.count
      retardation =  incidents.where(:incident_type_id => 20).count
      incidents_data_attributes["delays_total"] = incidents.where(:incident_type_id => 20).count + incidents.where(:incident_type_id => 26, :previous_incident_type_id => 20).count
      incidents_data_attributes["absences_start"] = incidents.absences_year.count
      
      if incidents_from_admission > 0
        incidents_data_attributes["absences_percent"] = number_to_percentage(incidents_data_attributes["absences_total"].to_f / incidents_from_admission * 100, :precision => 0)
        incidents_data_attributes["delays_percent"] = number_to_percentage(incidents_data_attributes["delays_total"].to_f / incidents_from_admission * 100, :precision => 0)
      else
        incidents_data_attributes["absences_percent"] = "0%"
        incidents_data_attributes["delays_percent"] = "0%"
      end
      
      incidents_data_attributes
    end
    
    def self.formatHours(hours)
      return "" if hours.blank?
      hours_array = (hours*60).divmod(60)
      minutes = hours_array[1].round(0)
      if minutes == 60
        minutes = 0
        hours_array[0] = hours_array[0] + 1
      end
      hours = hours_array[0]<10? "0#{hours_array[0]}" : hours_array[0]
      minutes = minutes<10? "0#{minutes}" : minutes
      return "#{hours}:#{minutes}"
    end
    
    def get_last_incidents(payroll)
      results = {}
  
  
      if enterprise.payrolls.opened.any?
        if payroll == 0
          last_closed_pay = enterprise.payrolls.closed.where(payroll_description_id:1).last
          payment_period = (last_closed_pay.nil?)? 0 : last_closed_pay.payment_period
          next_payroll = enterprise.payrolls.where('payment_period_id > ? AND payroll_description_id =?',payment_period, 1).first.payment_period.initial_date
          
          schedule_days = []
          schedule.schedule_days.each do |schedule_day|
            schedule_days[schedule_day.day_id] = schedule_day
          end
  
  
          results["incidents"] = incidents.select("incidents.*, name, with_salary_gose, letters")
          .joins(:incident_type)
          .where("date >= ?", next_payroll).map do |incident|        
            wday = incident.date.wday
            wday = 7 if wday == 0
            
            previous_incident_type = (incident.previous_incident_type_id.nil?)? nil : IncidentType.find(incident.previous_incident_type_id).name
  
  
            if incident.user.employee_id.nil?
              user = ((incident.user.employee_id.nil?)? incident.user.to_s : '<a title="' + incident.user.employee.to_s + '" target="_blank" href="' + employee_path(incident.user.employee) + '">' + incident.user.employee.employee_number + '</a>')
            else
              user = incident.user.to_s 
            end
            
            hours_schedule = (schedule_days[wday].nil?)? 0.0 : schedule_days[wday].total_hours
            hours_worked = (incident.hours_worked.blank?)? 0.0 : incident.hours_worked
            extra_hours_exceded = ((hours_worked - hours_schedule ) > 0)? hours_worked - hours_schedule : 0
  
  
            incident.attributes.merge({
                date_formatted: "#{I18n.t(incident.date.strftime("%A").downcase)} #{incident.date.mday}    ("+I18n.t('months_general.'+incident.date.strftime("%B"))+")",
                date: incident.date.to_s,
                incident_name: incident.incident_type.name,
                user: user,
                previous_incident_name: previous_incident_type,
                schedule_hours: Employee.formatHours(hours_schedule),
                extra_hours_worked_formatted: Employee.formatHours(incident.extra_hours_worked),
                hours_worked_formatted: Employee.formatHours(hours_worked),
                extra_hours_added_formatted: Employee.formatHours(incident.extra_hours_added), 
                extra_hours_exceded: Employee.formatHours(extra_hours_exceded),
                is_delete:  schedule.schedule_days.pluck(:day_id).include?(wday)? false : true
              })
          end
        else
          payroll = Payroll.where(:id => payroll).last
  
  
  
          schedule_days = []
          plan_schedules =  plan_employee_schedules.where(:payment_period_id => payroll.payment_period_id).last
  
          plan_schedules = plan_schedules.schedule if !plan_schedules.blank?
          plan_schedules = schedule if plan_schedules.blank?
          
          plan_schedules.schedule_days.each do |schedule_day|
            schedule_days[schedule_day.day_id] = schedule_day
          end
  
  
          results["incidents"] = incidents.select("incidents.*, name, with_salary_gose, letters")
          .joins(:incident_type)
          .where("date >= ? and date <= ?", payroll.payment_period.initial_date, payroll.payment_period.final_date).map do |incident|        
            wday = incident.date.wday
            wday = 7 if wday == 0
            
            previous_incident_type = (incident.previous_incident_type_id.nil?)? nil : IncidentType.find(incident.previous_incident_type_id).name
  
  
            if incident.user.employee_id.nil?
              user = ((incident.user.employee_id.nil?)? incident.user.to_s : '<a title="' + incident.user.employee.to_s + '" target="_blank" href="' + employee_path(incident.user.employee) + '">' + incident.user.employee.employee_number + '</a>')
            else
              user = incident.user.to_s 
            end
            
            hours_schedule = (schedule_days[wday].nil?)? 0.0 : schedule_days[wday].total_hours
            hours_worked = (incident.hours_worked.blank?)? 0.0 : incident.hours_worked
            extra_hours_exceded = ((hours_worked - hours_schedule ) > 0)? hours_worked - hours_schedule : 0
  
  
            incident.attributes.merge({
                date_formatted: "#{I18n.t(incident.date.strftime("%A").downcase)} #{incident.date.mday}    ("+I18n.t('months_general.'+incident.date.strftime("%B"))+")",
                date: incident.date.to_s,
                incident_name: incident.incident_type.name,
                user: user,
                previous_incident_name: previous_incident_type,
                schedule_hours: Employee.formatHours((schedule_days[wday].nil?)? 0.0 : schedule_days[wday].total_hours),
                extra_hours_worked_formatted: Employee.formatHours(incident.extra_hours_worked),
                hours_worked_formatted: Employee.formatHours(incident.hours_worked),
                extra_hours_added_formatted: Employee.formatHours(incident.extra_hours_added), 
                extra_hours_exceded: Employee.formatHours(extra_hours_exceded), 
                is_delete:  plan_schedules.schedule_days.pluck(:day_id).include?(wday)? false : true
              })
          end        
        end
      else
  
        logger.info("+++++++++++++++++++++ No entre")
        results["incidents"] = Incident.none
      end
      
      results
    end
    
    
    def get_last_vacations(id)
        results = {}
        if department_work.payrolls.opened.any?
        schedule_days = []
        schedule.schedule_days.each do |schedule_day|
          schedule_days[schedule_day.day_id] = schedule_day
         end
        end
        results ["employees"]  = id
        results.merge!(get_rest_days).merge!(get_days_available)
    end
    
    def get_days_available
        results = {}
        results ["busy_days"]  = incidents.select("date")
        .where("date>current_date()")
        results
    end
  
    def get_rest_days_by_nexts_peridos
      results = {}
  
      results["daysDisabled"] = []
  
      date_current = Time.now()
      first_date = (date_current - 1.month).to_date
      last_date = (date_current + 6.month).to_date
      for date in first_date..last_date
        date_in_planed_schedule = false
          plan_employee_schedules.each { |plan_employee_schedule|
            if plan_employee_schedule.payment_period.initial_date <= date and
              plan_employee_schedule.payment_period.final_date >= date
  
              date_in_planed_schedule = true
  
              dates_disable_by_schedule_planed(plan_employee_schedule).each { |day|
                if date.wday == day
                  results["daysDisabled"] << date.strftime("%d/%m/%Y")  
                end  
              }
            end
          }
        if !date_in_planed_schedule
          dates_disable_by_schedule.each {|day|
            if date.wday == day
              results["daysDisabled"] << date.strftime("%d/%m/%Y")
            end
          } 
        end
      end
      results["holidays_dates"] = department_work.company_branch.company.holidays.pluck(:date).map { |date| date.strftime("%d/%m")}
      
      if !enterprise.payrolls.closed.blank?
        results["last_payroll_opened_date"] = enterprise.payrolls.closed.last.payment_period.final_date + 1.day
      else
        results["last_payroll_opened_date"] = enterprise.payrolls.first.payment_period.initial_date
      end
      results
  
    end
  
    def get_last_payroll_open
      results = {}
  
      if !enterprise.payrolls.closed.blank?
        results["last_payroll_opened_date"] = enterprise.payrolls.closed.where(payroll_description_id: 1).last.payment_period.final_date + 1.day
      else
        results["last_payroll_opened_date"] = enterprise.payrolls.where(payroll_description_id: 1).first.payment_period.final_date + 1.day
      end
      results
    end
  
    def get_first_date
      result = ""
  
      if !enterprise.payrolls.closed.blank?
        result = enterprise.payrolls.closed.where(payroll_description_id: 1).last.payment_period.final_date + 1.day
      else
        result = first_admission_date
      end
      result
    end
  
  
    def dates_disable_by_schedule_planed(plan_employee_schedule)
      schedule_day_ids = plan_employee_schedule.schedule.schedule_days.pluck(:day_id).map { |wday|
        wday = 0 if wday == 7
        wday      
      }
       wdays = 0..6
      days = []
      
      wdays.each { |wday|
        wday = 0 if wday == 7
        days << wday if schedule_day_ids.exclude?(wday)
      }
  
      days
    end
  
    def dates_disable_by_schedule
      schedule_day_ids = schedule.schedule_days.pluck(:day_id).map { |wday|
        wday = 0 if wday == 7
        wday      
      }
       wdays = 0..6
      days = []
      
      wdays.each { |wday|
        wday = 0 if wday == 7
        days << wday if schedule_day_ids.exclude?(wday)
      }
  
      days
    end
  
    def get_rest_days
      results = {}
      
      results["daysOfWeekDisabled"] = dates_disable_by_schedule
  
      results["holidays_dates"] = department_work.company_branch.company.holidays.pluck(:date).map { |date| date.strftime("%d/%m")}
      
      results["last_payroll_opened_date"] = enterprise.payrolls.opened.first.payment_period.initial_date
      
      results
    end
    
    def get_current_schedule_day(is_departure=false, registration_time)
      date = registration_time.to_date
      date_aux = date
      date_day_id = date.wday
      date_day_id = 7 if date_day_id == 0
  
    
      payment_period = PaymentPeriod.with_date(date, date, self.company_process_model).first
      planned_schedule = nil
      is_incomplete = false
  
      planned_schedule = self.schedule
  
  
      if !payment_period.nil?
        plan_schedule = plan_employee_schedules.find_by_payment_period_id(payment_period.id)
        if !plan_schedule.nil?
          planned_schedule = plan_schedule.schedule 
        end
  
        if planned_schedule.nil?
          planned_schedule = self.schedule
        end
      end
      
      schedule_day = planned_schedule.schedule_days.select do |s|
        if s.is_nightly?
          date_aux = date
          if !is_departure
            #date_aux = date - 1.day
            #date_aux = date
            date_day_id = date_aux.wday
            date_day_id = 7 if date_day_id == 0
          end
  
          entry_time = s.entry_time.change(year: date_aux.year, month: date_aux.month, day: date_aux.day , hour: 0)
          departure_time = entry_time
          departure_time = s.departure_time.change(year: departure_time.year, month: departure_time.month, day: departure_time.day , hour: 23, min:59, sec: 59)
        else
          entry_time = s.entry_time.change(year: date.year, month: date.month, day: date.day, hour: 0) 
          departure_time = s.departure_time.change(year: date.year, month: date.month, day:  date.day, hour: 23, min:59, sec: 59)
          
        end
        s.day_id == date_day_id && entry_time <= registration_time && departure_time >= registration_time
        
      end.first
  
       
  
      schedule_day_attributes = schedule_day.attributes.merge(
        "date" => date_aux,
        "start_tolerance" => planned_schedule.start_tolerance,
        "end_tolerance" => planned_schedule.end_tolerance,
        "meal_start_time" => planned_schedule.meal_start_time,
        "meal_end_time" => planned_schedule.meal_end_time,
        "schedule_is_nightly" => schedule_day.is_nightly?,
        "schedule_is_active" => schedule_day.schedule.is_active?
      ) if !schedule_day.nil?
      
      schedule_day_attributes
      
    end
    
    def get_date_reading_assistances(date)
      date_day_id = date.wday
      date_day_id = 7 if date_day_id == 0
      reading_assistances_array = {}
    
      payment_period = PaymentPeriod.with_date(date, date, self.company_process_model_id).first
  
      
      if !payment_period.nil?
        plan_schedule = plan_employee_schedules.find_by_payment_period_id(payment_period.id)
        if !plan_schedule.nil?
          schedule = plan_schedule.schedule 
          
        end
        if schedule.nil?
          schedule = self.schedule
        end
      else
        schedule = self.schedule
      end
  
      schedule = schedule
      
      #schedule.schedule_days
      #.where("schedule_days.day_id = ?", date_day_id).each do
        reading_assistances_array[schedule.schedule_type] = []
        load_type = []
        if schedule.is_run_schedule
          load_type = ["entry","departure", "extra_entry", "extra_departure"]
        else
          load_type = ["entry", "meal_start", "meal_end", "departure", "extra_entry", "extra_departure"]
        end
        load_type.each do |cause|
          reading_assistance = reading_assistances
          .find_by(date: date, cause: cause)
          if reading_assistance.nil?
            reading_assistance_attributes = {
              "cause" => cause,
              "registration_time_to_s" => ""
            }
          else
            reading_assistance_attributes = reading_assistance.attributes
            reading_assistance_attributes["registration_time_to_s"] = I18n.l(reading_assistance.registration_time, :format => :hour)
          end
          
  
  
          reading_assistance_attributes["cause_t"] = I18n.t(cause).capitalize
          reading_assistances_array[schedule.schedule_type] << reading_assistance_attributes
        end
  
      #end
      is_rest_day = false
  
  
      if schedule.schedule_days.where("schedule_days.day_id = ?", date_day_id).blank?
        is_rest_day = true
      end
  
  
  
      reading_assistances_array[:is_rest_day] = is_rest_day

      reading_assistances_array
    end
    
    def create_card
      require "imgkit"
      url_options = ActionMailer::Base.default_url_options
      host_with_port = "#{url_options[:host]}:#{url_options[:port]}"
      card_front_kit = Tempfile.new(["card_front_#{id}", ".png"], "tmp", :encoding => "ascii-8bit")
      card_back_kit = Tempfile.new(["card_back_#{id}", ".png"], "tmp", :encoding => "ascii-8bit")
      card_front_kit.write(IMGKit.new("http://#{host_with_port}/employees/card_front/#{id}",  :width => 306, :height => 482).to_png)
      card_back_kit.write(IMGKit.new("http://#{host_with_port}/employees/card_back/#{id}",  :width => 306, :height => 482).to_png)
      card_front_kit.flush
      card_back_kit.flush
      update_attribute(:card_front, card_front_kit)
      update_attribute(:card_back, card_back_kit)
      card_front_kit.unlink
      card_back_kit.unlink
    end
    
    def set_only_schedule(schedule_id)
      update(schedule_id: schedule_id)
    end
    
    def has_departure_at?(date)
      reading_assistances.where(date: date, cause: "departure").any?
    end
    
    def has_assistance_at?(date)
      incidents.joins(:incident_type).where("date = ? AND incident_types.letter = 'A'", date).any?
    end
    
    def has_incident_at?(date)
      incidents.where("date = ?", date).any?
    end
    
    def missing_data(current_user)
      fields = []
      
      (attribute_names - %w{card_front_file_name card_front_content_type 
        card_front_file_size card_front_file_size card_front_updated_at 
        card_back_file_name card_back_content_type card_back_file_size 
        card_back_updated_at photo_file_name photo_content_type photo_file_size 
        photo_updated_at signature_file_name signature_content_type signature_file_size 
        signature_updated_at}).each do |name|
        fields << I18n.t("activerecord.attributes.employee.#{name}") if send(name).blank?
      end
      
      {
        "fields" => fields.join(", "),
        "documents" => current_user.company.document_structures
        .where.not(:id => employees_document_structures.where.not(:document_file_name => nil).pluck(:document_structure_id))
        .pluck(:name).join(", ")
      }
    end
  
  
    def desactivate_fonacot_loans
      if !self.active
        actives = self.fonacot_loans.where(:active => true);
        
        if !actives.blank?
          actual_period = PaymentPeriod.where('company_process_model_id = ?  AND final_date >= ?', self.company_process_model_id , Date.today).first
          actual_payroll = self.enterprise.payrolls.where(:payment_period_id => actual_period).first.id
          
          actives.each do |loan|
            payroll_to_delete = loan.fonacot_payrolls.where('payroll_id > ?', actual_payroll)
            payroll_to_delete.each do |pd|
              deduction =  self.employees_payrolls_deductions.where(:key_deduction => '011', :payroll_id => pd.payroll_id).first
              deduction.delete if !deduction.nil?
            end
            loan.update(:active => false)
          end
        end
      end
    end
  
    def desactivate_infonavit_credit
      
      if !self.active
        infonavit_credits = self.infonavit_credits.where(:active=>true)
        infonavit_credits.each do |credit|
          credit.update(:active => false) 
        end
      else
        if self.changed.include?'company_process_model_id'
          infonavit_credits = self.infonavit_credits.where(:active=>true)
          infonavit_credits.each do |credit|
            previous_company_process_model = CompanyProcessModel.find_by_id(self.changes['company_process_model_id'].first)
            previous_process = previous_company_process_model.process_model
  
            pays = (30.4/self.company_process_model.process_model.frequency_days).to_i
  
            previous_pays = (30.4/previous_process.frequency_days).to_i
  
            if credit.period_discount.to_i * previous_pays == credit.discount.to_i
              discount = credit.discount.to_f/pays
              credit.update(:period_discount => discount)
            else
              discount = (credit.period_discount.to_f*previous_pays)/pays
              credit.update(:period_discount => discount)
            end
          end
        end
      end
    end
  
    def fonacot_loan_report
      fonacot_loan_deduction = []
      start_date = self.first_admission_date
      end_date = Date.today
          loans = self.fonacot_loans
          if !loans.blank?
            
            #Selecciona los periodos en los cuale se encuentra un préstamo para ese empleado
            payrolls = []
         
            loans_a = loans.map do |loan|
              amount = 0
              pay_amount = 0
              paids = 0
              total_loan = 0
              loan.payrolls.each do |payroll|
                period = payroll.payment_period
                if period.final_date >= start_date and period.final_date < end_date
                  amount += payroll.fonacot_payrolls.find_by(:fonacot_loan_id => loan.id).amount
                  if end_date <= Date.today     
                    paids += 1
                  else
                    if period.final_date <= Date.today
                      paids += 1  
                    end
                  end                
                elsif period.final_date < start_date
                  paids += 1
                  pay_amount += payroll.fonacot_payrolls.find_by(:fonacot_loan_id => loan.id).amount   
                else
                  total_loan += payroll.fonacot_payrolls.find_by(:fonacot_loan_id => loan.id).amount     
                end                         
              end
              
              m=[]
              pre_d = loan.previous_date
              (0..(loan.total_payments-1)).each do |month|
                cpp = 0
                loan.payrolls.each do |payroll|
                  if payroll.payment_period.final_date <= pre_d
                    cpp += 1
                  end
  
                 
                end
                pre_d += 1.month
                m[month] = cpp
              end
  
              pos = 0
              closed = paids
              if m.include?(closed)
                pos = m.index(closed)+1
              else
                m.each do |p|
                  if closed > p
                    pos = m.index(p)+1
                  end
                end
              end
  
              total_loan += amount + pay_amount
  
              {
                'credit_number' => loan.credit_number,
                'amount' => loan.total_amount,
                'ret_mensual' => loan.discount ,
                'term' => loan.total_payments,
                'ret_real' => amount ,
                'paid' => pos, 
                'rest_amount' => total_loan - pay_amount,
                'pay_amount' => amount + pay_amount,
                'rest_to_date' => total_loan -= amount + pay_amount   
              }
            end
  
            fonacot_loan_deduction << { 
              "fonacot_client" => self.fonacot_client,
              "RFC" => self.rfc,
              "employee" => self.fullname_reverse,
              "employee_number" => self.employee_number,
              "loans" => loans_a
            }
  
          end
        {
          "start_date" => start_date,
          "end_date" => end_date,
          "enterprise" => self.id,
          "enterprise_work_center" => '1231231231',
          "enterprise_loans" => fonacot_loan_deduction
        }
    end
  
    def change_enterprise_employee
  
      if self.changed.include?'enterprise_id' or self.changed.include?'company_process_model_id'
        actives = self.fonacot_loans.where(:active => true);
        if !actives.blank?
          if self.changed.include?'enterprise_id' and self.changed.include?'company_process_model_id'
            enterprise = self.changes['enterprise_id'].first
            model = self.changes['company_process_model_id'].first
          elsif self.changed.include?'company_process_model_id'
            enterprise = self.enterprise
            model = self.changes['company_process_model_id'].first
          else
            enterprise = self.changes['enterprise_id'].first
            model = self.company_process_model_id
          end
  
          actual_period = PaymentPeriod.where('company_process_model_id = ?  AND final_date >= ?', model , Date.today).first
          actual_payroll = Payroll.where('payment_period_id >= ? AND enterprise_id = ? AND closed = ?', actual_period, enterprise, false).first if !actual_period.nil?
          
  
  
          actives.each do |loan|
              payroll_to_delete = ''
              
              if actual_payroll.payment_period.final_date > Date.today
                payroll_to_delete = loan.fonacot_payrolls.where('payroll_id >= ?', actual_payroll) if !actual_payroll.nil?
              else
                payroll_to_delete = loan.fonacot_payrolls.where('payroll_id > ?', actual_payroll) if !actual_payroll.nil?
              end
              totals = 0
              payroll_to_delete.each do |pd|
                if self.enterprise.is_subcontractor or (self.company_process_model.id != model)
                  deduction =  self.employees_payrolls_deductions.where(:key_deduction => '011', :payroll_id => pd.payroll_id).first
                  totals += pd.amount
                  deduction.delete if !deduction.nil?
                  pd.delete
                end
              end
              if self.company_process_model.id != model
                pays = ((totals/loan.discount).to_i)
                mod = (totals%loan.discount)
                
                initial_date = loan.finish_date - pays.month
                previous_date = initial_date
                
                  (0..pays).each do |pay|
                    final_date_aux =  previous_date + pay.month
                    
                    if (initial_date < Date.today) or initial_date == final_date_aux
                      initial_date = Date.today - 1.day
                    end
                    
                    periods = PaymentPeriod.where('company_process_model_id = ?  AND final_date < ? AND final_date >= ? AND final_date > ?',
                      self.company_process_model , final_date_aux, initial_date, Date.today)
  
                    payrolls = periods.map do |period| 
                      self.enterprise.payrolls.where(:payment_period => period.id, :closed => false)
                    end
                    
                    if initial_date < Date.today
                      discount = mod/ payrolls.count if mod > 0.009
                      discount = loan.discount/payrolls.count if mod < 0.009
                    else
                      discount = loan.discount/ payrolls.count
                    end
  
  
                    payrolls.each do |payroll|
                      FonacotPayroll.create(:fonacot_loan_id => loan.id, :payroll_id => payroll.first.id, :amount => discount)
                    end
                    
                    initial_date = final_date_aux
                  end
                end
              end 
        end
      end
    end
    
    private
  
    def check_schedule
      plans = PlanEmployeeSchedule.get_currente_process(id).first
      if !plans.blank?
        plans.skip_validation = true
        plans.update(:schedule_id=>schedule_id)
      end
    end
  
    def company_process_model_id_payrolls
      if PaymentPeriod.where('company_process_model_id = ?  AND final_date >= ?', self.company_process_model_id , Date.today).first.nil?
        errors.add(:company_process_model_id, ", no se puede actualizar por que no existen los periodos de pago de este tipo de proceso.") 
      end
    end
  
    def unique_employee_number
      if !department_work.nil?
        if department_work.company_branch.employees.exists?("employee_number = #{employee_number}")
          errors.add(:employee_number, I18n.t("employee_number_already_exists"))
        end
      end
    end
    
    
    def desactivate_user
      user.update(active: false)
    end
  
  
    def set_employee_number
      user_branch_key = false
  
      if !self.enterprise.company.parameters.where(id: 39).blank?
        if self.enterprise.company.company_parameters.find_by(:parameter_id => 39).value == "1.0"
          user_branch_key = true
        end if !self.enterprise.company.company_parameters.where(:parameter_id => 39).blank?
      end
  
      if user_branch_key
        prev_number = '0000'
  
        if !self.enterprise_branch.blank?
          employees = department_work.company_branch.employees.where(:enterprise_branch_id => self.enterprise_branch)
          ent_id = self.enterprise_branch.key#.to_s.length
          ent_id_length = ent_id.to_s.length
          (0..(3-ent_id_length)).each {|d| ent_id = '0'+ent_id.to_s }
          prev_number = ent_id
        else
          employees = department_work.company_branch.employees.where(:enterprise_id => self.enterprise_id)
          ent_id = self.enterprise_id#.to_s.length
          ent_id_length = self.enterprise_id.to_s.length
          (0..(2-ent_id_length)).each {|d| ent_id = '0'+ent_id.to_s }
          prev_number = 'E'+ent_id
        end
  
        if !employees.blank? 
          if !department_work.company_branch.employee_number_char.blank?
            last_employee_number = employees.sort_by { |e| e.employee_number[1..-1].to_i }.map {|e| e.employee_number}
          else
            last_employee_number = employees.sort_by { |e| e.employee_number.to_i }.map {|e| e.employee_number}
            
            new_number = last_employee_number.last.to_i + 1
            
            (0..(4-new_number.to_s.length)).each {|d| new_number = '0'+new_number.to_s }
  
            next_employee_number = "#{department_work.company_branch.employee_number_char}#{prev_number}#{new_number}"        
          end
        else
            next_employee_number = "#{department_work.company_branch.employee_number_char}#{prev_number}00001"
        end
      else
        if !department_work.company_branch.employee_number_char.blank?
          last_employee = department_work.company_branch.employees.sort_by { |e| e.employee_number[1..-1].to_i }.last
        else
          last_employee = department_work.company_branch.employees.sort_by { |e| e.employee_number.to_i }.last
        end
  
  
        if !last_employee.nil?
          last_employee_number = last_employee.employee_number
          if last_employee_number =~ /^[a-zA-Z]/
            next_employee_number = last_employee_number[0] + "%05d" % (last_employee_number[1..-1].to_i + 1)
          else
            next_employee_number = "%05d" % (last_employee_number.to_i + 1)
          end
        else
          next_employee_number = "#{department_work.company_branch.employee_number_char}00001"
        end
      end
      
      self.employee_number = next_employee_number
    end
    
    def strip_attributes
      attribute_names.each do |name|
        attribute = send(name)
        send("#{name}=", attribute.strip) if attribute.is_a?(String)
      end
    end
  
  
    def key_regment? 
      regimen_type_id == 1
    end
  
    def size_bank_number?
      if bank_account_number.size.equal?(10) 
        false
      elsif bank_account_number.size.equal?(11) 
        false
      elsif bank_account_number.size.equal?(16) 
        false
      elsif bank_account_number.size.equal?(18)
        false
      else
        true
      end
    end
  end
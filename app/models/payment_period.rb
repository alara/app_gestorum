class PaymentPeriod < ActiveRecord::Base

    #SCOPES
    default_scope { order(:initial_date) }
    
    scope :has_another_payroll, ->(initial_date){
      joins('left join company_process_models ON company_process_models.id = payment_periods.company_process_model_id')
      .joins('left join process_models ON process_models.id = company_process_models.process_model_id')
      .where('process_models.key=?',"99")
      .where('payment_periods.initial_date=?',initial_date)
      
    }
    scope :current_year_a, ->{
      where("initial_date >=?", '01/01/'+Date.today.year.to_s)
    }
  
    scope :current_date, ->{
      where("initial_date>?",Date.today)
    }
  
    scope :last_date, ->(payment){
      where("initial_date>?",payment.final_date)
    }
  
    scope :specific, ->(period_id, date){
      where("company_process_model_id = #{period_id} AND initial_date <= ? and final_date >= ?", date, date)
    }
  
    scope :with_date, ->(initial_date, final_date, process){
      where("initial_date <=? AND final_date >=? AND company_process_model_id = ? ",final_date, initial_date, process)
    }
  
    
    #ATTRIBUTES
    attr_accessor :year, :normal_save
    
    #RELATIONS
    belongs_to :company_process_model
    has_many :payrolls
  
    has_many :plan_employee_schedules
  
    #VALIDATIONS
    validates :company_process_model_id, :year, :presence => true
  
    #METHODS  
    def select_display
      "#{initial_date} - #{final_date}"
    end
  
    def number_process
      "#{number_payment}"
    end
  
    def plan_period
      "#{company_process_model.process_model.unity} [#{number_payment}] #{initial_date} - #{final_date}"
    end
    
    def to_s
      select_display
    end
  
    def save_only_one
    end
  
    def self.create_extra(attribute_names = self.attribute_names)
        sql = "INSERT INTO payment_periods (`company_process_model_id`, `initial_date`, `final_date`, number_payment) VALUES (#{attribute_names[:company_process_model_id]},'#{attribute_names[:initial_date]}','#{attribute_names[:final_date]}',1);"
        ActiveRecord::Base.connection.execute(sql) 
        payment_period = PaymentPeriod.find_by_sql("SELECT * FROM payment_periods").last
        payment_period
    end
  
  
    def save
      if normal_save.blank?
        if company_process_model.blank?
          errors.add(:company_process_model, :empty)
          return false
        elsif year.blank?
          errors.add(:year, :empty)
          return false
        else
          years = company_process_model.payment_periods.where(" YEAR(initial_date)=?", year).where(" YEAR(final_date)=?", year)
          if years.any?
            errors.add(:base, I18n.t("payment_period_already_exists", process: company_process_model.to_s, year: year))
            return false
          end
        end
        result = false
        period_wday = company_process_model.initial_day.to_i
        period_wday = 0 if period_wday == 7
        self.year = year.to_i
        number_period = 1
        case company_process_model.process_model_id
        when 8
          periods = []
          periods.push("(#{company_process_model_id},'#{initial_date.to_formatted_s(:db)}','#{final_date.to_formatted_s(:db)}','#{number_period}')")
          result = insert_periods(periods)
        when 1
          year_first_day = Date.new(year)
          year_first_day_wday = year_first_day.wday.to_i
          if(period_wday < year_first_day_wday)
            year_first_day-=(year_first_day_wday-period_wday)
          elsif(period_wday > year_first_day_wday)
            year_first_day+=(period_wday-year_first_day_wday)
          end
          periods = []
          period_initial_day = year_first_day
          period_final_day = period_initial_day + 6
          if PaymentPeriod.where(company_process_model_id: company_process_model_id)
            .where(initial_date: period_initial_day.to_formatted_s(:db))
            .where(final_date: period_final_day.to_formatted_s(:db)).any?
            period_initial_day += 7
            period_final_day = period_initial_day + 6
          end
          while ((period_initial_day.year == year)||(period_final_day.year == year)) do
            periods.push("(#{company_process_model_id},'#{period_initial_day.to_formatted_s(:db)}','#{period_final_day.to_formatted_s(:db)}','#{number_period}')")
            number_period += 1
            period_initial_day += 7
            period_final_day = period_initial_day + 6
          end
          if PaymentPeriod.where(company_process_model_id: company_process_model_id)
            .where(initial_date: period_initial_day.to_formatted_s(:db))
            .where(final_date: period_final_day.to_formatted_s(:db)).any?
            periods.delete_at(periods.length-1)
          end
          result = insert_periods(periods)
        when 3
          result = generate_period(10)
        when 2
          result = generate_period(15) 
        when 11 
          periods = []
          initial_year_day = Date.new(2017)
          month = 1
          while (month <= 12)
            period_initial_day = initial_year_day
            initial_year_day = initial_year_day + 2.month
            period_final_day = initial_year_day - 1.day
            periods.push("(#{company_process_model_id},'#{period_initial_day.to_formatted_s(:db)}','#{period_final_day.to_formatted_s(:db)}','#{number_period}')")
            month += 2
          end
          result = insert_periods(periods)
        when 10
          #### Periodo catorcenal ####
          year_first_day = Date.new(year)
          year_first_day_wday = year_first_day.wday.to_i
          if(period_wday < year_first_day_wday)
            year_first_day-=(year_first_day_wday-period_wday)
          elsif(period_wday > year_first_day_wday)
            year_first_day+=(period_wday-year_first_day_wday)
          end
          periods = []
          period_initial_day = year_first_day
          period_final_day = period_initial_day + 13
          if PaymentPeriod.where(company_process_model_id: company_process_model_id)
            .where(initial_date: period_initial_day.to_formatted_s(:db))
            .where(final_date: period_final_day.to_formatted_s(:db)).any?
            period_initial_day += 14
            period_final_day = period_initial_day + 13
          end
          while ((period_initial_day.year == year)||(period_final_day.year == year)) do
            periods.push("(#{company_process_model_id},'#{period_initial_day.to_formatted_s(:db)}','#{period_final_day.to_formatted_s(:db)}','#{number_period}')")
            number_period += 1
            period_initial_day += 14
            period_final_day = period_initial_day + 13
          end
          if PaymentPeriod.where(company_process_model_id: company_process_model_id)
            .where(initial_date: period_initial_day.to_formatted_s(:db))
            .where(final_date: period_final_day.to_formatted_s(:db)).any?
            periods.delete_at(periods.length-1)
          end
          result = insert_periods(periods)
        when 4
          periods = []
          months_with_31 = [1,3,5,7,8,10,12]
          month = 1
          while (month <= 12) do
            period_initial_day = Date.new(year,month)
            if months_with_31.include?(month)
              period_final_day = Date.new(year,month,31)
            elsif month==2
              period_final_day = Date.new(year,3)-1
            else
              period_final_day = Date.new(year,month,30)
            end
            periods.push("(#{company_process_model_id},'#{period_initial_day.to_formatted_s(:db)}','#{period_final_day.to_formatted_s(:db)}','#{number_period}')")
            number_period += 1
            month+=1
          end
          result = insert_periods(periods)
        end
        return result
      else
        super
      end    
    end
    
    def generate_period(frecuency)
      periods = []
      months_with_31 = [1,3,5,7,8,10,12]
      month = 1
      number_period = 1
      while (month <= 12) do
        incremental_day = 1
        while (incremental_day <= 21) do
          period_initial_day = Date.new(year,month,incremental_day)
          if incremental_day >= 16
            if months_with_31.include?(month)
              period_final_day = Date.new(year,month,31)
            elsif month==2
              period_final_day = Date.new(year,3)-1
            else
              period_final_day = Date.new(year,month,30)
            end
          else
            period_final_day = period_initial_day + (frecuency - 1)
          end
          periods.push("(#{company_process_model_id},'#{period_initial_day.to_formatted_s(:db)}','#{period_final_day.to_formatted_s(:db)}','#{number_period}')")
          number_period += 1
          incremental_day += frecuency
        end
        month+=1
      end
      return insert_periods(periods)
    end
    
    def insert_periods(periods)
      sql = "INSERT INTO payment_periods (`company_process_model_id`, `initial_date`, `final_date`, `number_payment`) VALUES #{periods.join(", ")};"
      return ActiveRecord::Base.connection.execute(sql)    
    end
  
  scope :get_dispersion_enterprise, -> (payroll_id,id_payment, active_dispe){
       select("ent.id as id_enterprise, ent.commercial_name,count(ep.employee_id) as total_employee,count(dpw.id) as t_deparment,
               round(sum(0 + 
               (COALESCE((CASE 
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0) + ep.period_bonus)-ep.loans),4) as dispersar")
        .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
        .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
        .joins("left join employees emp on emp.id = ep.employee_id")
        .joins("left join enterprises ent on ent.id = ep.enterprise_id")
        .joins("left join department_works dpw on dpw.id = ep.department_work_id") 
        .joins("left join banks bnk on bnk.id = emp.bank_id")
        .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
        .where("payment_periods.id = #{id_payment} ")
        .where("pay.id= #{payroll_id}")
        .group("ent.id")
    }
  
  
  
    scope :get_department_dispersion, -> (payroll_id,id_enterprise,id_payment, active_dispe){
      select("dpw.id as dept_id,dpw.name, dpw_pc.name as pf_name, count(pay.total_employees) as empleados")
            .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
            .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
            .joins("left join employees emp on emp.id = ep.employee_id")
            .joins("left join enterprises ent on ent.id = ep.enterprise_id")
            .joins("left join department_works dpw on dpw.id = ep.department_work_id") 
            .joins("left join profit_centers dpw_pc ON dpw_pc.id = dpw.profit_center_id") 
            .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise}")
            .where("pay.id= #{payroll_id}")
            .group("dpw.id")
     }
  
    scope :get_employee_dispersion, -> (payroll_id,id_payment,id_enterprise,id_department,active, active_dispe){
     select("emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,dpw.name,payment_periods.initial_date,payment_periods.final_date,
            round(0 + (COALESCE((CASE 
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0)), 4) as dispersar,bnk.short_name") 
            .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
            .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
            .joins("left join employees emp on  emp.id = ep.employee_id")
            .joins("left join department_works dpw on dpw.id = ep.department_work_id")
            .joins("left join banks bnk on bnk.id = emp.bank_id")
            .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
            .joins("left join enterprises ent on ent.id = ep.enterprise_id")
            .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
            .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department}
                   ")
            .where("pay.id= #{payroll_id}")
            .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
            .order("emp.last_name")
          }
  
    scope :get_employee_dispersion_dif, -> (payroll_id,id_payment,id_enterprise,id_department,active, active_dispe){ 
      select("emp.id, emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,dpw.name,payment_periods.initial_date,payment_periods.final_date,
            round((ep.real_salary *(ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
             - (COALESCE((CASE 
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0)), 4) as dispersar,bnk.short_name") 
            .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
            .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
            .joins("left join employees emp on  emp.id = ep.employee_id")
            .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
            .joins("left join department_works dpw on dpw.id = ep.department_work_id")
            .joins("left join banks bnk on bnk.id = etx.bank_id")
            .joins("left join enterprises ent on ent.id = ep.enterprise_id")
            .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
            .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department}
                   ").order("emp.last_name") 
            .where("pay.id= #{payroll_id}")
            .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
    }
  
  
    scope :get_dispersion_bank, -> (payroll_id,id_payment,id_enterprise,id_department,id_bank, active_dispe){
     select("emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,
             dpw.name,payment_periods.initial_date,payment_periods.final_date,ent.issuing_banorte,
            round( 
            (COALESCE((CASE 
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0) ), 4) as dispersar,bnk.short_name") 
            .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
            .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
            .joins("left join employees emp on  emp.id = ep.employee_id")
            .joins("left join department_works dpw on dpw.id = ep.department_work_id")
            .joins("left join banks bnk on bnk.id = emp.bank_id")
            .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
            .joins("left join enterprises ent on ent.id = ep.enterprise_id")
            .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
            .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department} and bnk.id = #{id_bank}
                    ")
            .where("pay.id= #{payroll_id}")
            .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
  
   }
  
    scope :get_dispersion_bank_dif, -> (payroll_id,id_payment,id_enterprise,id_department,id_bank, active_dispe){
       select("emp.id, emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,
               dpw.name,payment_periods.initial_date,payment_periods.final_date,ent.issuing_banorte,
              round((ep.real_salary * (ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
               - (COALESCE((CASE
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0)), 4) as dispersar,bnk.short_name")
              .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
              .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
              .joins("left join employees emp on  emp.id = ep.employee_id")
              .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
              .joins("left join department_works dpw on dpw.id = ep.department_work_id")
              .joins("left join banks bnk on bnk.id = etx.bank_id")
              .joins("left join enterprises ent on ent.id = ep.enterprise_id")
              .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
              .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department} and bnk.id = #{id_bank}
                      ")
              .where("pay.id= #{payroll_id}")
              .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
    }
  
    scope :get_employee_dispersion_both, -> (payroll_id,id_payment,id_enterprise,id_department,active, active_dispe){ 
      select("emp.id, emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,dpw.name,payment_periods.initial_date,payment_periods.final_date,
            round((ep.real_salary *(ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
                , 4) as dispersar,bnk.short_name") 
                .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
                .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
                .joins("left join employees emp on  emp.id = ep.employee_id")
                .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
                .joins("left join department_works dpw on dpw.id = ep.department_work_id")
                .joins("left join banks bnk on bnk.id = etx.bank_id")
                .joins("left join enterprises ent on ent.id = ep.enterprise_id")
                .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
                .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department}
                       ").order("emp.last_name") 
              .where("pay.id= #{payroll_id}")
                .where("(case when #{active_dispe} then ep.bank_id = etx.bank_id when #{(!active_dispe)} then true end)") 
    }
  
    scope :get_dispersion_bank_both, -> (payroll_id,id_payment,id_enterprise,id_department,id_bank, active_dispe){
       select("emp.id, emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,
               dpw.name,payment_periods.initial_date,payment_periods.final_date,ent.issuing_banorte,
              round((ep.real_salary * (ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
               , 4) as dispersar,bnk.short_name")
              .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
              .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
              .joins("left join employees emp on  emp.id = ep.employee_id")
              .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
              .joins("left join department_works dpw on dpw.id = ep.department_work_id")
              .joins("left join banks bnk on bnk.id = etx.bank_id")
              .joins("left join enterprises ent on ent.id = ep.enterprise_id")
              .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
              .where("payment_periods.id = #{id_payment} and ent.id = #{id_enterprise} and dpw.id = #{id_department} and bnk.id = #{id_bank}
                      ")
              .where("pay.id= #{payroll_id}")
              .where("(case when #{active_dispe} then ep.bank_id = etx.bank_id when #{(!active_dispe)} then true end)")
    }
  
    scope :get_dispersion_total, -> (payroll_id, active_dispe){
     select("emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,
             dpw.name,payment_periods.initial_date,payment_periods.final_date,ent.issuing_banorte,
            round( 
            (COALESCE((CASE 
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0) ), 4) as dispersar,bnk.short_name") 
            .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
            .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
            .joins("left join employees emp on  emp.id = ep.employee_id")
            .joins("left join department_works dpw on dpw.id = ep.department_work_id")
            .joins("left join banks bnk on bnk.id = emp.bank_id")
            .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
            .joins("left join enterprises ent on ent.id = ep.enterprise_id")
            .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
            .where("pay.id = #{payroll_id}")
            .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
  
   }
  
    scope :get_dispersion_bank_dif_total, -> (payroll_id, active_dispe){
       select("emp.id, emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,
               dpw.name,payment_periods.initial_date,payment_periods.final_date,ent.issuing_banorte,
              round((ep.real_salary * (ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
               - (COALESCE((CASE
                  WHEN #{active_dispe} AND tde.imss_total > 0 THEN tde.imss_total
                  WHEN ep.cash >= 0 then ep.cash
                  WHEN (ep.cash = 0  OR ep.cash is null) then (select total_payment from employee_payroll_payroll_types where payroll_type_id = 1 and employee_payroll_id = ep.id)
                  END ),0)), 4) as dispersar,bnk.short_name")
              .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
              .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
              .joins("left join employees emp on  emp.id = ep.employee_id")
              .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
              .joins("left join department_works dpw on dpw.id = ep.department_work_id")
              .joins("left join banks bnk on bnk.id = etx.bank_id")
              .joins("left join enterprises ent on ent.id = ep.enterprise_id")
              .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
              .where("pay.id = #{payroll_id}")
              .where("(case when #{active_dispe} then ep.bank_id != etx.bank_id when #{(!active_dispe)} then true end)")
    }
  
    scope :get_employee_dispersion_both_total, -> (payroll_id, active_dispe){ 
      select("emp.id,  emp.employee_number,emp.first_name,emp.last_name,ep.bank_account_number,dpw.name,payment_periods.initial_date,payment_periods.final_date, ent.issuing_banorte,
            round((ep.real_salary *(ep.total_worked_days + (ep.sevent_day) - ep.time_discount))+
               (COALESCE((select total_extra_hours from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select bonus from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select vacations from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select settlement from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select infonavit from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select fonacot from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select loans from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select saving_fund from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))+
               (COALESCE((select saving_fund_pay from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))-
               (COALESCE((select discount from taxes_data_employees where employee_id=emp.id and payroll_id= ep.payroll_id), 0))
              , 4) as dispersar,bnk.short_name") 
              .joins("left join payrolls pay on pay.payment_period_id = payment_periods.id")
              .joins("left join employee_payrolls ep on ep.payroll_id = pay.id")
              .joins("left join employees emp on  emp.id = ep.employee_id")
              .joins("LEFT JOIN  enterprise_taxeds etx ON emp.enterprise_taxed_id = etx.id")
              .joins("left join department_works dpw on dpw.id = ep.department_work_id")
              .joins("left join banks bnk on bnk.id = etx.bank_id")
              .joins("left join enterprises ent on ent.id = ep.enterprise_id")
              .joins("left join taxes_data_employees tde ON tde.payroll_id = ep.payroll_id  and tde.employee_id = emp.id")
              .where("pay.id = #{payroll_id}")
              .where("(case when #{active_dispe} then ep.bank_id = etx.bank_id when #{(!active_dispe)} then true end)") 
    }
  
  
  end
class ScheduleDay < ActiveRecord::Base
  #SCOPES
  default_scope { order(:day_id) }
  
  #ASSOCIATIONS
  belongs_to :day
  belongs_to :schedule
  has_many :reading_assistances
  #VALIDATIONS
  validates :day_id, :entry_time, :departure_time, :total_hours, :presence => true
   
  #METHODS
  def select_display
    "#{I18n.t(day.name)} #{entry_time} - #{departure_time}"
  end
  
  def is_nightly?
    entry_time > departure_time
  end
end

class CompanyBranch < ActiveRecord::Base
    #ASSOCIATIONS
    belongs_to :company
    belongs_to :banks
    has_many :users, :dependent => :restrict_with_error
    has_many :department_works, :dependent => :restrict_with_error
    has_many :sub_department_crews, :through => :department_works
    has_many :employees, :through => :department_works, :source => :employees
    has_many :loans, :through => :employees
    has_many :schedules
    has_many :schedule_days, :through => :schedules
    has_many :reading_assistances, :through => :employees
    has_many :incidents, :through => :employees
    has_many :reading_assistances_loads, :dependent => :destroy
    has_many :payrolls, :through => :department_works
    
    #VALIDATIONS
    validates :company_id, :presence => true
    validates :zip, numericality: { only_integer: true }, length: { is: 5 }
    
    #METHODS
    def select_display
      commercial_name
    end
  
    def to_s
      commercial_name
    end
  end
  
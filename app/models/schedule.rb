class Schedule < ActiveRecord::Base
    #SCOPES
    default_scope { order(:schedule_type) }
    scope :actives, -> {where('is_active = 1') }
    #ATTRIBUTES
    attr_accessor :start_tolerance_formatted, :end_tolerance_formatted, :break_time_formatted
    
    #ASSOCIATIONS
    belongs_to :company_branch
    belongs_to :day
    belongs_to :work_day
    has_many :schedule_days, :dependent => :destroy
    has_many :employees, :dependent => :restrict_with_exception
    has_many :days, :through => :schedule_days
    accepts_nested_attributes_for :schedule_days, :allow_destroy => true, :reject_if => proc { |attributes| attributes["entry_time"].blank? && attributes["departure_time"].blank? }
    
    
  
    def select_display
      schedule_type
    end
    
    def to_s
      schedule_type
    end
  end
  
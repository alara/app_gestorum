class User < ApplicationRecord
  devise :database_authenticatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlackList

         #ASSOCIATIONS
      belongs_to :role
      belongs_to :company_branch
      belongs_to :enterprise_branch
      belongs_to :employee
      has_many :reading_assistances
      has_many :reading_assistances_loads


      def photo_url
        if photo_file_name.blank?
          "/assets/no-image.png"
        else
          "/system/users/photos/000/"+get_number(id.to_i)+"/medium/#{photo_file_name}"
        end
      end
  
      def get_number id
          value = "%06d" % id.to_s
          value = value.insert(3, '/')
          url = ""
          value.split('').each do |v| 
            url += v
          end
          url
      end
end

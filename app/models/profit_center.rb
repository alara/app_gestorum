class ProfitCenter < ActiveRecord::Base

	belongs_to :employee
  has_many :department_works
	has_many :subdepartments, :through =>  :department_works

  has_many :budget_paths, :as => :budgetable #polymorphic 

	default_scope { order(:name) }
	def to_s
    	name
  	end

    def select_display
      name
    end

  	def date_range
  		"#{initial_date} - #{final_date}"
  	end

  	def check_dates?
  		result = true
  		if self.initial_date.to_formatted_s(:db)>=self.final_date.to_formatted_s(:db)
  			errors.add(:base, "#{self.initial_date.to_formatted_s(:db)} debe ser menor que #{self.final_date.to_formatted_s(:db)}")
  			result = false
  		end
  		result
  	end



  	def name_up
	    self.name = self.name.upcase
  	end
end

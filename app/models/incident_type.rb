class IncidentType < ActiveRecord::Base
    #SCOPES
    default_scope { order(:name) }
    
    #CALLBACKS
    before_save :check_params
    before_destroy :incident_type_cannot_be_removed
    before_validation -> { self.color = "#000000" if !color.present? }
    
    #RELATIONS
    belongs_to :company
    has_many :accident_types
    
    #VALIDATIONS
    validates :name, :letters, :unit_time, :amount, :color, :presence => true
    validates :name, uniqueness: { scope: :company_id }
    validates :letters, uniqueness: { scope: :company_id }
    validates :letters, length: { in: 1..2}
    validates :amount, numericality: true, allow_blank: true
    
    #METHODS
    def select_display
      name
    end
    
    def to_s
      name
    end  
    
    private
    
    def check_params
      self.letters.upcase!
    end
    
    def incident_type_cannot_be_removed
      if company_id.nil?
        errors.add(:base, I18n.t("insident_type_cannot_be_removed"))
        return false
      end
    end
  end
  
class EnterpriseBranch < ActiveRecord::Base


    attr_accessor :total_employees_sub
  	#Relations
  	belongs_to :employee
  	belongs_to :enterprise
    belongs_to :profit_center
    belongs_to :state
    belongs_to :city
  	#belongs_to :employee

    has_many :employees
    scope :h_payroll, -> {
      where(:has_payroll => true)
    }

    scope :has_payroll, -> (id) {
      where(:enterprise_id => id, :has_payroll => true)
    }

    scope :hasnt_payroll_child, -> {
      where(:has_payroll => false)
    }

    scope :hasnt_payroll, -> (id, enterprise_id) {
      where(:parent_id => id, :enterprise_id=> enterprise_id, :has_payroll => false)
    }

    scope :hasnt_parent, -> (enterprise_id) {
      where(:parent_id => nil, :enterprise_id=> enterprise_id, :has_payroll => false)
    }



  	#VALIDAT-IONS
  	validates_presence_of :name, :enterprise_id, :enterprise_employer_registration_id
  	validates :zip, length: { is: 5 }, numericality: true

  	def select_display
  	  	name
    end

    def full_address
      address.to_s + " " + ext_number.to_s+ ((int_number.to_s.blank?)? "" : (" " +int_number.to_s))  + " Col. " + address_2.to_s
    end

    def to_s
    	name
    end
end

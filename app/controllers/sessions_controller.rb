class SessionsController < Devise::SessionsController
    respond_to :json

    def create
      self.resource = warden.authenticate!(auth_options)
      sign_in(resource_name, resource)
      yield resource if block_given?
        connect_user
        response  = ( @response[:success] ? success_auth(@response) : error_auth)
       render json: response
    end


  
    private

    def connect_user
      @connect = current_user.subdomain
      @connect = @connect.to_sym
      ActiveRecord::Base.connect_to(@connect) do
        user = User.find_by(email: current_user.email)
        photo  = user.photo_url

        @employee_number =  user.employee_id.blank? ? '' : Employee.find(user.employee_id).employee_number


        uses_services = (user.uses_services==1 ? true : false)
        @auto_check = ((user.active && uses_services) ? user.auto_check : false )
        @response = {
          success: (user.active && uses_services),
          photo: photo
        }
      end
    end

    def error_auth
      {
        error: true,
        message: "Usuario no autorizado, contacta a un administrador."
      }
    end

    def  success_auth user

      

      {
        error: false, 
        data: { 
          token: current_token , 
          user: {
             id: current_user.id,
             email: current_user.email,
             active: current_user.active,
             connect: current_user.subdomain,
             auto_check: @auto_check,
             photo: user[:photo],
             employee_number: @employee_number
          } 
         }, 
        message: "Authentication successful" 
      }
    end

    def current_token
      request.env['warden-jwt_auth.token']
    end
  
    def respond_with(resource, _opts = {})
      render json: resource
    end
  
    def respond_to_on_destroy
      head :no_content
    end
  end
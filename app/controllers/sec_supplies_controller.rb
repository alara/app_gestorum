class SecSuppliesController < ApplicationController
    respond_to :json
    before_action :authenticate_user!
    before_action :create_connect
    respond_to :json
    
    def index 
        ActiveRecord::Base.connect_to(@connect) do
        @sec_supplies = SecSupply.all
        json_response({
            records: @sec_supplies.map{|sec_supply| {
                id: sec_supply.id,
                name: sec_supply.name,
                code: sec_supply.code,
                description: sec_supply.description,
                } 
            }
        })
        end
    end

    def create 
        @sec_supply = SecSupply.new(sec_supply_params)
        json_response_save(@sec_supply)
    end

    def show
        json_response(format_json)
    end

    def edit
        json_response(format_json)
    end

    def update 
        @sec_supply.update(sec_supply_params)
        json_response_save(@sec_supply)
    end

    def destroy
        @sec_supply.update_attributes(:active => false)
        json_response_save(@sec_supply)
    end


    private

    def format_json 
        {
            id: @sec_supply.id,
            name: @sec_supply.name,
            code: @sec_supply.code,
            description: @sec_supply.description,
        }
    end

    def paginate_load
        paginate = (params[:page].blank? ? 1 : params[:page])
        @sec_supplies = SecSupply.paginate(:page => paginate, :per_page => ApplicationController::PER_PAGE)
    end

    def sec_supply_params
        params.permit(:name, :description, :code)
    end

    def set_sec_supply
        @sec_supply = SecSupply.find(params[:id])
    end


    private
    def create_connect
        @connect = params[:connect]
        @connect = @connect.to_sym
    end

end
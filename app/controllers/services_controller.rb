class ServiceController < ApplicationController
    respond_to :json
    before_action :authenticate_user!
    before_action :create_connect

    def get_employees
      ActiveRecord::Base.connect_to(@connect) do
        employees  = Employee.actives
        render json: success(employees.map{|employee|
          {
            id: employee.id, 
            fullname: employee.fullname,
            photo: "#{@connect.to_s}#{employee.photo_url}",
            has_enrolled: employee.has_enrolled
          }
        })
      end
    end

    def enrolled_employee
      render json: @connect
    end

    def get_auto_check
      ActiveRecord::Base.connect_to(@connect) do
        user = User.find_by(email: current_user.email)
        render json: {
          auto_check: user.auto_check
        }
      end
    end

    def get_data_employee
        ActiveRecord::Base.connect_to(@connect) do
         
            employee = User.find_by(email: current_user.email).company_branch.employees
            .find_by(employee_number: params[:employee_number])
            
            if employee.nil?
                employee_data = { "errors" => "No tiene empleado." }
            elsif employee.department_work_id.nil?
                employee_data = { "errors" => "El empleado se encuentra inactivo" }
            else
                employee_data = employee.get_data_attributes
            end

            render json: employee_data
        end
    end


      def post_reading_assistance
        ActiveRecord::Base.connect_to(@connect) do
        @current = User.find_by(email: current_user.email)
        
        logger.info "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#{params['employee_number']}"
        employee = Employee.find_by(employee_number: params[:employee_number])
        
        if employee.nil?
          json = { "errors" => {base: "No tiene empleado."} }
        elsif !employee.active
          json = { "errors" => {base: "El empleado se encuentra inactivo"}  }
        else
          reading_assistance = @current.reading_assistances.new(
            :employee_id => employee.id,
            :cause => params[:cause],
            :latitude => params[:latitude],
            :longitude => params[:longitude],
            :concept_id => nil,
            :registration_time => params[:registration_time]
          )
              
          if reading_assistance.save
            if params[:cause] == "departure" && !params[:schedule_id].blank?
              employee.set_only_schedule(params[:schedule_id])
            end
                  
            json = reading_assistance
          else
            json = { "errors" => reading_assistance.errors }
          end
        end
        
        cause = I18n.t('reports.payment_receipts_stamped.'+params[:cause])

        render json: {
          json: json,
          message: "La #{cause} se ha efectuado correctamente."
        }
        end
      end

      def sec_supplie_items
        ActiveRecord::Base.connect_to(@connect) do
          items = SecSupplieItem.all
          render json: {
            items: items
          }
        end
      end

      def sec_supplies
        ActiveRecord::Base.connect_to(@connect) do
          items = SecSupply.all
          render json: {
            items: items
          }
        end
      end


    private
    def create_connect
        @connect = params[:connect]
        @connect = @connect.to_sym
    end

end
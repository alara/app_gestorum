class RegistrationsController < Devise::RegistrationsController
    respond_to :json
  
    def create
      logger.info(sign_up_params.inspect)
      build_resource(sign_up_params)
  
      resource.save
      render_resource(resource)
    end
  end
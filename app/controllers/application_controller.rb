class ApplicationController < ActionController::API
    respond_to :json
    before_action :authenticate_user!

    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

    def render_not_found_response(exception)
      render json: { error: exception.message }, status: :not_found
    end

    def check_session
      render json: success(params[:token])
    end

    def test
      connect = params[:connect]
      ActiveRecord::Base.connect_to(connect.to_sym) do
        @employees = Employee.all
        render json: @employees.map{|employee|
        {
          id: employee.id,
          fullname: employee.fullname_reverse
        }
      }
      end
      
    end

    def json_response(data)
      render json: success(data)
    end

    def json_response_save(data)
      render json: (data.save ? success(data) : error(data.errors))
    end

    def render_resource(resource)
        if resource.errors.empty?
          render json: resource
        else
          validation_error(resource)
        end
      end
    
      def validation_error(resource)
        render json: error([
          {
            status: '400',
            title: 'Bad Request',
            detail: resource.errors,
            code: '100'
          }
        ]), status: :bad_request
      end
    
    def home
        render json: success([],I18n.t('message_default'))
    end


    def success data=[],message=""
        {
            error: false,
            data: data,
            message: ""
        }
    end

    def error data=[],message=""
        {
            error: true,
            data: data,
            message: ""
        }
    end
end
